The Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975

(Maharashtra Act No. 16 of 1975)

Last Updated 23rd March, 2021 \[mh280\]

* * *

For Statement of Objects and Reasons, _see Maharashtra Government Gazette_, 1975, Part V, page 378.

LEGISLATIVE HISTORY 6

*   Amended by Maharashtra 21 of 1976 (1-11-1976)
*   Amended by Maharashtra 13 of 1982 (20-4-1982)
*   Amended by Maharashtra 22 of 1985 (16-8-1985)
*   Amended by Maharashtra 25 of 1986 (3-7-1986)
*   Amended by Maharashtra 12 of 1987 (20-4-1987)
*   Amended by Maharashtra 9 of 1988 (22-4-1988)
*   Amended by Maharashtra 9 of 1989 (1-4-1989)
*   Amended by Maharashtra 24 of 19904 (28-8-1990)
*   Amended by Maharashtra 12 of 1991 (22-2-1991)
*   Amended by Maharashtra 11 of 1992 (1-5-1992)
*   Amended by Maharashtra 17 of 1993 (1-5-1993)
*   Amended by Maharashtra 12 of 1995 (8-6-1995)
*   Amended by Maharashtra 16 of 1995 (1-9-1995)
*   Amended by Maharashtra 19 of 1996 (29-6-1996)
*   Amended by Maharashtra 9 of 1997 (1-10-1996)
*   Amended by Maharashtra 30 of 1997 (15-5-1997)
*   Amended by Maharashtra 21 of 1998 (1-5-1998)
*   Amended by Maharashtra 25 of 1999 (1-4-1999)
*   Amended by Maharashtra 28 of 2000 (1-5-2000)
*   Amended by Maharashtra 51 of 2000 (1-5-2000)
*   Amended by Maharashtra 22 of 2001 (1-4-2001)
*   Amended by Maharashtra 16 of 2002 (26-6-2002)
*   Amended by Maharashtra 20 of 2002 (1-5-2002)
*   Amended by Maharashtra 13 of 2004 (1-7-2004)
*   Amended by Maharashtra 17 of 2006 (1-4-2006)
*   Amended by Maharashtra 32 of 2006 (20-6-2006)
*   Amended by Maharashtra 6 of 2007
*   Amended by Maharashtra 25 of 2007 (15-8-2007)
*   Amended by Maharashtra 5 of 2008 (22-2-2008)
*   Amended by Maharashtra 17 of 2009 (1-7-2009)
*   Amended by Maharashtra 12 of 2010 (1-5-2010)
*   Amended by Maharashtra 8 of 2012 (1-5-2012)
*   Amended by Maharashtra Act No. 31 of 2017
*   Amended by Maharashtra Act No. 42 of 2017
*   Amended by Maharashtra Act No. 26 of 2018
*   Amended by Maharashtra Act No. 14 of 2019, dated 9.7.2019
*   Amended by Maharashtra Act No. 16 of 2019, dated 9.7.2019
*   Amended by Maharashtra Act No. 20 of 2020
    

**An Act to provide for the levy and collection of a tax on professions, trades, callings and employments for the benefit of the State**

Whereas It is expedient to provide for the levy and collection of a tax on professions, trades, callings and employments for the benefit of the State \[\* \* \*\] and for matters connected therewith; it is hereby enacted in the Twenty-sixth Year of the Republic of India as follows:

**1\. Short title, extent and commencement.** - (1) This Act may be called the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975.

(2) It extends to the whole of the State of Maharashtra.

(3) it shall be deemed to have come in force on the 1st day of April 1975.

**2\. Definitions.** - In this Act, unless the context otherwise requires -

(a) _"Commissioner"_ means the Commissioner of Profession Tax appointed under section 12, and includes \[the Special Commissioner of Profession Tax and\] an Additional Commissioner of Profession Tax (if any) appointed under that section;

(b) _"corporation area"_ means an area within the limits of a municipal corporation constituted under the Bombay Municipal Corporation Act, the Bombay Provincial Municipal Corporations Act, 1949 or the City of Nagpur Corporation Act, 1948;

\[(ba) _"employee"_ means a person employed on salary or wages, and includes -

(i) a Government servant receiving pay from the revenues of the Central Government or any State Government or the Railway Fund;

(ii) a person in the service of a body, whether incorporated or not, which is owned or controlled by the Central Government or any State Government where the body operates in any part of the State, even though its headquarters may be outside the State;

(iii) a person engaged in any employment of an employer, not covered by items (i) and (ii) above;\]

(c) _"employer"_, in relation to an employee earning any salary or wages on regular basis under him, means the person or the officer who is responsible for disbursement of such salary or wages and includes the head of the office of any establishment as well as the manager or agent of the employer; the person or the officer who is responsible for paying salary or wages of the employee. It includes the head of the office of any establishment and that of the employer. The purpose of this definition is to fix the responsibility to deduct the professional tax from the employee's salary and pay to the Government.

\[ca) _"engaged"_, in relation to any profession, trade, calling or employment, means occupied fully or otherwise in such profession, trade, calling or employment, whether any pecuniary benefit or benefit of any nature whatsoever; actually accrues or not to a person from such occupation;\]

(d) _"month"_ means a month reckoned according to the British calendar;

(e) _"person"_ means any person who is engaged \[actively or otherwise\] in any profession, trade, calling or employment in the State of Maharashtra and includes a Hindu undivided family, firm, company, corporation or other corporate body, any society, club or association, so engaged, but does not include any person who earns wages on a casual basis;

(f) _"prescribed"_ means prescribed by the rules made under this Act;

(g) _"profession tax"_ or _"tax"_ means the tax on professions, trades, callings and employments levied under this Act;

(h) _"salary"_ or _"wages"_ includes pay or wages, dearness allowance and all other remunerations received by any person on regular basis, whether payable in cash or kind, and also includes perquisites and profits in lieu of salary, as defined in section 17 of the Income-Tax Act, 1961 \[but does not include bonus in any form and on any account or gratuity\];

(i) _"Schedule"_ means a Schedule appended to this Act;

\[(j) _"Tribunal"_ means the Maharashtra Sales Tax Tribunal constituted under section 11 of the Maharashtra Value Added Tax Act, 2002 and discharging the functions of the Tribunal assigned to it by or under this Act;\]

(k) _"year"_ means the financial year.

**3\. Levy and charge of tax.** - (1) Subject to the provisions of Article 276 of the Constitution of India and of this Act, there shall be levied and collected a tax on professions, trades, callings and employments for the benefit of the State.

(2) Every person \[excluding firms (whether registered under the Indian Partnership Act, 1932, or not) and Hindu undivided family\] engaged \[actively or otherwise\] in any profession, trade, calling or employment and falling under one or the other of the classes mentioned in the second column of Schedule I shall be liable to pay to the State Government the tax at the rate mentioned against the class of such persons in the third column of the said Schedule:

Provided that the tax so payable in respect of any one person shall not exceed \[two thousand and five hundred rupees\] in any year:

Provided further that, entry \[21\] in Schedule I shall apply only to such classes of persons as may be specified by the State Government by notification in the _Official Gazette,_ from time to time.

\[Provided also that, a person who is liable to pay tax has remained un-enrolled; then, his liability to pay tax under this section for the periods for which he has remained so un-enrolled shall not exceed eight years from the end of the year immediately preceding the year in which he has obtained the enrolment certificate or the year in which the proceeding for enrolment is initiated against him, whichever is earlier.\]

\[exceed :

(a) four years, in case where certificate of enrollment is granted on or after the 1st April 2017,

(b) eight years, in any other case, from the end of the year immediately preceding the year in which certificate of enrollment has been granted, or the year in which the proceeding for enrollment is initiated against him, whichever is earlier.\]

**4\. Employer's liability to deduct and pay tax on behalf of employees.** - The tax payable under this Act by any person earning a salary or wage, shall be deducted by his employer from the salary or wage payable to such person, before such salary or wage is paid to him, and such employer shall, irrespective of whether such deduction has been made or not, when the salary or wage is paid to such persons be liable to pay tax on behalf of all such persons:

Provided that, if the employer is an officer of Government, the State Government may, notwithstanding anything contained in this Act, prescribe by rules the manner in which such employer shall discharge the said liability:

\[Provided further that, where any person earning a salary or wage-

(a) is also covered by one or more entries other than entry 1 in Schedules I and the rate of tax under any such other entry is more than the rate of tax under entry 1 in that Schedule, or

(b) is simultaneously engaged in employment of more than one employer, and such person furnishes to his employer or employers a certificate in the prescribed form declaring, _inter alia_, that he shall get enrolled under sub-section (2) of section 5 and pay the tax himself, then the employer or employers of such person shall not deduct the tax from the salary or wage payable to such person and such employer or employers, as the case may be, shall not be liable to pay tax on behalf of such person\].

**\[4A. Restricted liability to pay tax.** - An employer, who has been granted certificate of registration on or after the 1st April 2017, shall not be liable to pay tax for a period of more than four years from the end of the year immediately preceding the year in which the certificate of registration has been granted or the year in which the proceeding for registration is initiated against him, whichever is earlier.**4B. Liability to deduct and pay tax in certain cases.** - (1) The State Government may, from time to time, by notification published in the _Official Gazette_, provide for the class of persons who shall deduct the tax out of the amount of the commission paid or payable to an agent, to be mentioned in the said notification, before the payment of such commission. The person so notified to deduct the tax shall pay the tax, in the manner provided in the said notification and at the rate specified in entry 1A of SCHEDULE I of the Act.

(2) All the provisions related to an employer and employee under the Act shall apply mutatis mutandis to the person liable to deduct the tax and the person from whom such tax is to be deducted under subsection (1).\]

**\[4C. Collection and payment towards tax.** - (1) The State Government may, from time to time, by notification published in the _Official Gazette_ and subject to such conditions and restrictions as may be specified therein, require any notified person or class of persons (hereinafter, in this section referred to as "tax collector"), who is eligible to receive any amount towards supply of goods or, as the case may be, supply of services or towards any other purposes, to collect an amount towards tax from an enrolled person or a person, liable to be enrolled.

(2) The tax collector shall pay such amount into the Government Treasury in the manner specified in the notification. The amount so paid under this section shall be deemed to have been paid on behalf of such person, from whom the said amount by way of tax is collected.

(3) All the provisions under the Act, related to an employer and employee, shall _mutatis mutandis_ apply to the person liable to collect the tax and the person from whom such tax is collected under sub-section (1).\]

**5\. Registration and enrolment.** - (1) Every employer (not being an officer of Government) liable to pay tax under section 4 \[or under sub-section (5) of section 10A\] shall obtain a certificate of registration from the prescribed authority in the prescribed manner.

(2) Every person liable to pay tax under this Act (other than a person earning salary or wages, in respect of whom the tax is payable by his employer), shall obtain a certificate of enrolment from the prescribed authority in the prescribed manner.

\[(2A) Notwithstanding anything contained in this section and the last preceding section, where a person is a citizen of India and is in employment of any diplomatic or consular office of Trade Commissioner of any foreign country situated in any part of the State, such person, if liable to pay tax, shall obtain a certificate of enrolment as provided in sub-section (2) and pay the tax himself.\]

(3) Every employer or person required to obtain a certificate of registration or enrolment shall, within thirty days from the date of first publication of this Act in the _Official Gazette,_ or, if he was not engaged in any profession trade, calling or employment on that date, \[within thirty days of his becoming liable to pay tax\] or, in respect of a person referred to \[in sub-section (2) or (2A) within thirty days\] of his becoming liable to pay tax at a rate higher or lower than the one mentioned in his certificate of enrolment, apply for a certificate of registration or enrolment, or a revised certificate of enrolment, as the case may be, to the prescribed authority in the prescribed form and the prescribed authority shall, after making such inquiry as may be necessary within thirty days of the receipt of the application (which period in the first year from the commencement of this Act shall be extended to ninety days), if the application is in order, grant him such certificate:

\[Provided that, where on account of revision of rate of tax the person liable to pay tax is required to pay tax at a rate higher or lower than the one mentioned in the certificate of enrolment, the rate of tax mentioned in such certificate shall be deemed to have been revised accordingly on the date of such revision of rate of tax as aforesaid; and pending such person applying for a revised certificate of enrolment and grant of such certificate to him, such person shall, notwithstanding anything contained in this Act, be liable to pay tax at such revised rate.\]

\[(3A) Notwithstanding anything contained in this section, a company, which has been incorporated under the provisions of the Companies Act, 2013, after the date of commencement of the Maharashtra State Tax on Professions, Trades, Callings and Employments (Amendment) Act, 2020, shall at the time of its incorporation, obtain the certificate of enrollment and certificate of registration under this Act.\]

(4) The prescribed authority shall mention in every certificate of enrolment, the amount of tax payable by the holder according to Schedule I, and the date by which it shall be paid, and such certificate shall \[subject to the provisions of the provisio to sub-section (3)\], serve as a notice of demand for purposes of section 10.

\[(5) Where an employer, liable to registration has failed to apply for such certificate within the required time, the prescribed authority may, after giving him a reasonable opportunity of being heard, impose penalty of rupees five for each day of delay in case of such employer.\]

(6) Where an employer or a person liable to registration or enrolment has \[\* \* \*\] given false information in any application submitted under this section, the prescribed authority may, after giving him a reasonable opportunity of being heard, impose a \[penalty equal to three times the tax payable under the Act\].

**6\. Returns.** - (1) Every employer registered under this Act shall furnish to the prescribed authority \[a return in such form; for such periods and by such dates as may be prescribed\] showing therein the salaries and wages paid by him and the amount of tax deducted by him in respect thereof. \[\* \* \*\].

\[Provided that, the employer registered under sub-section (3A) of section 5 shall, after the commencement of his liability to pay tax, furnish to the prescribed authority, a return in such form, for such period and by such dates as may be prescribed.\]

\[Provided that, the Commissioner may, subject to such terms and conditions, if any as may be prescribed, permit any employer to furnish a consolidated return relating to all or any of the places of business of such employer in the State, for such period or periods, to such authority, as he may direct.\]

(2) Every such return shall be accompanied by a treasury challan in proof of payment of full amount of tax due according to the return, and a return without such proof of payment shall not be deemed to have been duly filed.

\[(3) Where an employer has failed to file such return within the prescribed time, he shall pay, by way of a late fee, an amount of \[rupees two hundred, in case he files the return within a period of thirty days after the expiry of the prescribed time for filing of such return and an amount of rupees one thousand, in any other case,\] before filing of the said return, This amount shall be in addition to the amount payable, if any, as per the return.\]

\[(4) Any employer liable to file return, having furnished a return, -

(a) discovers any omission or incorrect statement therein, may furnish, a revised return in respect of the period covered by the return, at any time before a notice for assessment is served on him in respect of the period covered by the said return or before the expiry of a period of six months from the end of the year to which such return relates, whichever is earlier;

(b) agrees with the findings contained in any intimation received by him as a result of-

(i) audit under section 7A; or

(ii) inspection under section 18,

then he may furnish a revised return as per the findings of audit or as the case may be, inspection within thirty days from the date of receipt of such intimation.\]

**\[7. Assessment and collection of tax.** - (1) The amount of tax due from an employer liable to pay tax shall be assessed separately for each year:

Provided that, the Commissioner may, subject to such conditions as may be prescribed and for reasons to be recorded in writing, assess the tax due from any employer for part of year:

Provided further that, when an employer has failed to furnish, by the prescribed date, any return relating to any period in any year, the Commissioner may, if he thinks fit, assess the tax due from such employer separately for different parts of such year.

(2) The Commissioner on being satisfied that, the returns furnished by an employer in respect of any period are correct and complete, he shall assess the amount of tax due from the employer on the basis of such returns:

Provided that, the Commissioner on not being satisfied that the returns furnished by the employer in respect of any period are correct and complete, and he is of the opinion that it is necessary to require the presence of the employer or the production of further evidence in that respect, he shall serve on such employer, in the prescribed manner, a notice requiring such employer on a date and at a place specified in the notice, either to attend and produce or cause to be produced all the evidence on which such employer has relied in support of his returns, or to produce such evidence as is specified in the notice. On the date specified in the notice, or as soon as may be thereafter, the Commissioner shall, after considering all the evidence produced, assess the amount of tax due from the employer:

Provided further that, if the employer fails to comply with any of the directions or requirements specified in the notice issued under the above proviso, the Commissioner may, assess, the amount of tax due from the employer on the basis of the record available before him.

(3) Where all the returns are filed by the employer for any year starting on or after the 1st April 2004 within one month from the end of the year to which such returns relate, no order of assessment under the provisos to sub-section (2) in respect of that year shall be made after the expiry of three years from the end of the said year; and if for any reason such order is not made within the period aforesaid, then the returns so filed shall be deemed to have been accepted as correct and complete for assessing the tax due from such employer:

Provided that, in the case of returns pertaining to the years ending on or before the 31st March, 2004, and filed on or before the 30th September, 2004, no order of assessment shall be made under the provisos to sub-section (2) on or after the \[1st April, 2008\]:

Provided further that, where a fresh assessment has to be made to give effect to any finding or direction contained in any order made under this Act, or to any order of the Tribunal or Court, such assessment shall be made within thirty-six months from the date of communication to the Commissioner of such finding, direction or order, as the case may be:

Provided also that, in computing the period specified in the second proviso, the time during which the assessment remained stayed by or under the order of the Tribunal or Court, shall stand excluded.

(4) Notwithstanding anything contained in this section or any other provisions of this Act, where the assessment involves a decision on a point which is concluded against the State by the judgment of the Tribunal and the State Government or the Commissioner has initiated any proceedings against such judgment before an appropriate forum, in such a case, the Commissioner may complete the assessment as if the point was not so decided by the Tribunal against the State, but shall stay the recovery of such dues including interest and penalty, if any, in so far as they relate to such point, until the decision by the appropriate forum and after such decision, modify the assessment order, in accordance with such decision.

(5) In any case where it comes to the notice of the Commissioner that, any employer has not furnished returns in respect of any period by the prescribed date, the Commissioner may, at any time, before the expiry of the period of eight years from the end of the year to which such period relates, after giving such defaulting employer a reasonable opportunity of being heard in that matter, assess, to the best of his judgment, the tax, if any, due from such employer.

(6) The Commissioner on being satisfied that an employer liable to pay tax in respect of any period, has failed to apply for registration within time as required by this Act, the Commissioner shall, at anytime, before the expiry of the period of eight years from the end of the year to which such period relates, after giving such defaulting employer, a reasonable opportunity of being heard, assess, to the best of his judgment, the tax, if any, due from the employer in respect of that period, and any period or periods subsequent thereto.

(7) The amount of tax so assessed shall be paid within fifteen days of receipt of the notice of demand from the prescribed authority.\]

**\[7A. Application of provisions of section 22 of Maharashtra Value Added Tax Act, 2002 and certain Provisions of Rules made thereunder.** - Subject to the provisions of this Act and the rules made thereunder in this behalf, the provisions related to audit under section 22 of the Maharashtra Value Added Tax Act, 2002 and the provisions of the rules made thereunder, so far as they relate to the electronic filing of returns and electronic payment of tax, or any amount payable under this Act, shall _mutatis mutandis_ apply for the purposes of this Act.\]

**8\. Payment of tax.** - (1) The tax payable under this Act shall be paid in the prescribed manner.

\[(2) The amount of tax due from an enrolled person, as specified in his enrolment certificate, shall be paid for each year on or before the 31st March of the said year :

Provided that, in respect of the person who is enrolled and the rate of tax at which he is liable to pay tax is revised, then such revised tax shall be paid on or before the 31st March of the year in which the rates are revised.\]

\[(3) The State Government may, by notification published in the _Official Gazette_, subject to such conditions and restrictions, as may be specified therein, provide a scheme for payment of tax, in advance, at the rate, which shall be lower than the rate applicable to an enrolled person under Schedule I.

(3-A) The liability of such enrolled person, who has paid such amount in advance, as per the notification issued under sub-section (3) shall not be varied due to any increase or decrease in the rate of tax, as provided in Schedule I.\]

\[(4) (a) A registered employer furnishing returns as required by sub-section (1) of sections 6 shall first pay into the Government Treasury, the amount of tax due from him for the period covered by a return alongwith the amount of interest payable by him under section 9 of the Act in such manner and at such intervals as may be prescribed.

(b) The amount of tax assessed under section 7 or found due under section 14 or 15 in respect of any period less any sum already paid by the employer or person in respect of such period shall be paid by the employer or person liable therefor into the Government Treasury within fifteen days from the date of service of notice of demand issued by the Commissioner in respect thereof.

Provided that, the Commissioner may, in respect of any particular employer or person and for reasons to be recorded in writing, allow him to pay tax, penalty or interest, if any, by instalment, but such grant of instalment to pay tax shall be without prejudice to levy of penalty or interest or both.\]

**9\. Consequences of failure to deduct or to pay tax.** - (1) If an employer (not being an officer of Government) \[fails to pay the tax\] as required by or under this Act, he shall without prejudice to any other consequences and liabilities which he may incur, be deemed to be an assessee in default in respect of the tax.

(2) Without prejudice to the provisions of sub-section (1) \[an employer referred to in that sub-section shall be liable\] to \[pay by way of simple interest, in addition to the amount of such tax, a sum calculated at the prescribed rate on the amount of such tax, for each month or part thereof, after the last date by which he should have paid the tax.\]

\[(2A) Where the amount of tax as assessed under section 7 or as found payable in view of an order passed under section 13 or 14 is more than the amount of tax paid by the employer, then such employer shall be liable to pay simple interest on the amount of difference of tax at the rate and in the manner laid down in sub-section (2)\].

(3) If an enrolled person fails to pay the tax as required by or under this Act, he shall be liable to pay simple interest at the rate and in the manner laid down in sub-section (2).

\[(3A) If a person, liable to get enrolled, fails to apply for certificate of enrolment within the period specified under this Act, he shall be liable to pay simple interest at the rate of 1.25 per cent. per month or part thereof of the amount of tax payable, from the 1st July of that year, till the date of payment of such tax, in addition to the amount of tax payable in respect of the year, for which he has remained unenrolled.\]

\[(4) The Commissioner may, subject to such conditions and limitations as may be prescribed, and for reasons to be recorded in writing, remit the whole or any part of the interest payable in respect of any period under this section.\]

**10\. Penalty for non-payment of tax.** - If an enrolled person or a registered employer fails without reasonable cause, to make payment of any amount of tax within the required time or date as specified in the notice of demand, the prescribed authority may, after giving him a reasonable opportunity of being heard, impose upon him \[a penalty equal to ten per cent, of the amount of tax due.\]

**\[10A. Special provision regarding liability to pay tax in certain cases.** - (1) Where an employer liable to pay tax under section 4 of this Act dies then, his legal representative shall be liable to pay tax (including any penalty and interest) due from such employer under this Act, in the like manner and to the same extent as the deceased employer, whether such tax (including any penalty and interest) has been assessed before the death of the employer but has remained unpaid, or is assessed after the death of the employer.

_Explanation._ - In this sub-section, the expression "legal representative" has the same meaning assigned to it in clause (11) of section 2 of the Code of Civil Procedure, 1908.

(2) Where an employer liable to pay tax under section 4 of this Act, is a Hindu undivided family and the joint family property is partitioned, amongst the various members or group of the members then, each member or group of members shall be jointly and severally liable to pay the tax (including any penalty and interest) due from the employer under this Act upto the time of partition, whether such tax (including any penalty and interest) has been assessed before the partition but has remained unpaid, or is assessed after partition.

(3) Where an employer liable to pay tax under this Act, is a firm, and the firm is dissolved then, every person who was a partner shall jointly and severally be liable to pay the tax (including any penalty and interest) due from the employer firm under this Act upto the time of dissolution, whether such tax (including any penalty and interest) has been assessed before such dissolution but has remained unpaid, or is assessed after such dissolution.

(4) Where an employer, liable to pay tax under this Act, transfers or otherwise disposes of his office or establishment or activity in whole or in part, or effects any change in employment in consequence of which he is succeeded in the office or establishment or activity or part thereof by any other person then, the employer and the person succeeding shall jointly and severally be liable to pay the tax (including any penalty and interest) due from the employer under this Act upto the time of such transfer, disposal or change, whether such tax (including any penalty and interest) has been assessed before such transfer; disposal or change but has remained unpaid or is assessed thereafter.

(5) Where an employer liable to pay tax under this Act, is succeeded in the office or establishment for activity by any person in the manner described in sub-section (4) then, such person shall,-

(a) notwithstanding anything contained in section 3, be liable to pay tax in respect of the period from the date of such succession, and

(b) within 30 days from the date of such succession, apply for certificate of registration, unless he already holds a certificate of registration.\]

**\[11. Recovery of tax.** - The provisions of the Maharashtra Goods and Services Tax Act, 2017, (Maharashtra XLIII of 2017) regarding recovery of tax shall, _mutatis mutandis_ apply for the recovery of dues under this Act.\]

**\[11A. Special powers of Profession Tax Authorities for recovery of tax as arrears of land revenue.** - (1) For the purpose of effecting recovery of the amount of tax, interest and penalty, due and recoverable from any person by or under the provisions of this Act, as arrears of land revenue, -

(i) the Commissioner of Profession Tax shall have and exercise all the powers and perform all the duties of the Commissioner under the Maharashtra Land Revenue Code, 1966 (hereinafter in this section referred to as "the said Code");

(ii) the Additional Commissioner of Profession Tax shall have and exercise all the powers and perform and all the duties of the Additional Commissioner under the said Code;

(iii) \[the Joint Commissioner\] of Profession Tax shall have and exercise all the powers and perform all the duties of the Collector under the said Code;

(iv) \[the Deputy Commissioner\] of Profession Tax shall have and exercise all the powers (except the powers of arrest and confinement of defaulter in a civil jail) and perform all the duties of the Assistant of Deputy Collector under the said Code;

(v) \[the Assistant Commissioner of Profession Tax and the Profession Tax Officer\] shall have and exercise all the powers (except the powers of confirmation of sale and arrest and confinement of defaulter in a civil jail) and perform all the duties of the Tahsildar under the said Code.

(2) Every notice issued or order passed in exercise of the powers conferred by sub-section (1) shall, for the purposes of sections 13, 14, 15 and 25, be deemed to be a notice issued or an order passed under this Act.\]

**12\. Authorities for implementation of the Act.** - (1) (a) For carrying out the purposes of this Act, the State Government may appoint,-

(i) an officer to be the Commissioner of Profession Tax for the whole of the State of Maharashtra;

(ii) \[a Special Commissioner of Profession Tax and one or more officers\] to be the Additional Commissioner of Profession Tax as the State Government thinks necessary;

\[(iii) such number of Joint Commissioners of Profession Tax, Deputy Commissioners of Profession Tax, Assistant Commissioners of Profession Tax, Profession Tax Officers and other officers and persons (with such designation) as the State Government thinks necessary;

(b) \[Special Commissioner, Additional Commissioners\], Joint Commissioners, Deputy Commissioners, Assistant Commissioners, \[,Profession Tax Officers and such other officers and persons (with such designation) as the State Government thinks necessary\] shall within the limit of such area as the Commissioner may specify by notification in the _Official Gazette,_ to be within their jurisdiction, exercise such powers and perform such duties of the Commissioner under this Act, as the Commissioner may, from time to time, by notification published in the _Official Gazette,_ delegate to them either generally or as respects any particular matter or class of matters.\]

(c) The superintendence and control for the proper execution of the provisions of this Act and the rules made thereunder relating to the levy and collection of the tax shall vest in the Commissioner.

(2) \[The Tribunal constituted under section 11 of the Maharashtra Value Added Tax Act, 2002\] shall be the Tribunal for the purposes of hearing appeals and revision applications and discharging other functions of the Tribunal under this Act, and accordingly, the provisions of section 21 of that Act (including any regulations made thereunder with such modifications, if any, therein as circumstances may require) and other provisions relating to the Tribunal under that Act shall also apply to and in relation to such Tribunal for the purposes of this Act.

(3) For carrying out the purposes of this Act, the State Government may, at its discretion appoint any Government Department or officer, or a Municipal Corporation, Municipal Council or _Zilla Parishad_ \[or any agency\] (hereinafter called "the Collecting Agent") as its agent responsible for levy and collection-of the tax under this Act, from such person or class of persons as may be prescribed; and thereupon, it shall be the duty of such Collecting Agent to carry out in such manner as may be prescribed, such functions under this Act as may be prescribed, and to render full and complete account of the tax levied and collected to the Commissioner in such manner and at such time as that officer may require.

\[(4) Any person authorised by the Collecting Agent in this behalf shall have for the purposes of levy and collection of the tax such powers as may be prescribed\].

(5) A Municipal Corporation, Municipal Council or \[_Zilla Parishad_ or agency\] appointed as agent to carry out the purposes of this Act under sub-section (3) shall be paid such collection charges as may be determined by the State Government, \[\* \* \*\].

(6) It shall be lawful for the Commissioner, or an Officer duly authorised by him, to have access to, and to cause production and examination of books, registers, accounts or documents maintained or required to be maintained by the Collecting Agent for the purposes of this Act, and the Collecting Agent shall, whenever called upon to do so, produce such books, registers, accounts or documents for inspection by the Commissioner or by the authorised officer.

\[(7) For carrying out the purposes of this Act, the Commissioner or an officer duly authorised by him may, appoint any person or persons, possessing such qualifications as may be prescribed, as his agent or agents (hereinafter referred to as the "recovery agent"). Such recovery agent shall be responsible for survey and recovery of the arrears of the tax (including interest and penalty) recoverable under this Act.

(8) It shall be the duty of the recovery agent to carry out such functions and in such manner, as may be prescribed, and to render full and complete account of the arrears recovered to the Commissioner or to the officer duly authorised by him, in such manner, and at such time, as that officer may require.

(9) The recovery agent shall have, for the purpose of survey and recovery of tax, interest and penalty, such powers as may be prescribed.

(10) The recovery agent shall be paid such incentive as may be determined by the Government.

(11) It shall be lawful for the Commissioner or an officer duly authorised by him, to have access to, and to cause production and examination of books, registers, accounts or documents maintained or required to be maintained by the recovery agent for the purpose of this Act, and the recovery agent shall, whenever called upon to do so, produce such books, registers, accounts or documents for inspection by the Commissioner or by the authorised officer.\]

**\[12A. Determination of certain disputed questions.** - (1) If any question arises, otherwise than in proceedings before a Court or before the prescribed authority has commenced assessment of an employer under section 7, about the interpretation or the scope of any expression defined in section 2 or of any entry in Schedule I, the Commissioner shall make an order determining such question.

_Explanation._ - For the purposes of this sub-section, the prescribed authority shall be deemed to have commenced assessment of an employer under section 7, when the employer is served with a notice under that section.

(2) The Commissioner may direct that the determination shall not affect the liability of any person under this Act, as respects the period prior to the determination.

(3) If any such question arises from any order already passed under this Act no such question shall be entertained for determination under this section, but such question may be raised in appeal against, or by way of revision of, such order\].

**13\. Appeal.** - (1) Subject to rules as may be made by the State Government, any person or employer aggrieved by any order made under sections 5, 6, 7, 9, 10, 15 or 16 may appeal against such order to, -

(a) \[the Deputy Commissioner\], if the order is passed by any prescribed authority or officer subordinate to him;

(b) \[the Joint Commissioner\], if the order is passed by the Assistant Commissioner; and

(c) the Tribunal, if the order is passed by any officer not below the rank of \[Joint Commissioner\].

(2) No appeal shall be entertained after the expiry of sixty days from the date of receipt of demand notice or receipt of the order:

Provided that, the appellate authority may admit the appeal after the expiry of the above period, if he is satisfied that there was sufficient cause for the delay.

\[(3) No appeal against an order of assessment with or without penalty or interest, or against an order imposing penalty or interest shall ordinarily be entertained by an appellate authority, unless such appeal is accompanied by satisfactory proof of the payment of tax with or without penalty or interest or, as the case may be, of the payment of penalty or interest, in respect of which appeal has been preferred:

Provided that, an appellate authority may, if it thinks fit, for reason to be recorded in writing, entertain an appeal against such order on payment of not less than twenty-five per cent, of the amount of tax, penalty or interest, in respect of which appeal has been preferred, as it may direct.\]

(4) The appellate authority in disposing of an appeal, may -

(i) confirm, annul, reduce, enhance, or otherwise modify the assessment or penalty or interest, or

(ii) set aside the assessment or penalty or interest and direct the authority which made the assessment or imposed the penalty or charged the interest to pass a fresh order after further inquiry on specified points.

(5) No order under this section shall be passed without giving the appellant or his representative, and, where the appellate authority is the Tribunal, without giving the authority whose order or directions is the subject of the appeal or his representative, a reasonable opportunity of being heard.

**14\. Revision.** - (1) Any order passed in appeal under section 13 may, on an application being made in this behalf, be revised by -

\[(a) the Joint Commissioner, if the order is passed by the Deputy Commissioner\];

(b) the Tribunal, if the order is passed by \[the Joint Commissioner\].

(2) Any order passed by \[the Joint Commissioner\] under sub-section (1) or by the Commissioner under sub-section (4) of this section, \[or any order made by the Commissioner under section 12A may, on an application being made to the Tribunal against such order, be revised by the Tribunal\].

(3) No revision shall be entertained under sub-section (1) or (2) after the expiry of sixty days from the date of the receipt of the order.

(4) The Commissioner may, of his own motion, revise any order passed by any authority other than the Tribunal under this Act:

Provided that, no order shall be revised by the Commissioner under this sub­section after the expiry of three years from the passing of the impugned order.

(5) No order under this section shall be passed without giving the applicant or the assessee a reasonable opportunity of being heard.

**15\. Rectification of mistakes.** - (1) Any authority under this Act may, of his own motion or on an application being made in this behalf, rectify any mistake apparent on the face of the record.

(2) Any authority under this Act may review his own order, if any employer has been under-assessed for any period:

Provided that, if an order under this section has an adverse effect on an employer or a person, no such order shall be passed unless a reasonable opportunity of being heard has been given to such employer or person:

Provided further that, no order under this section shall be passed after the expiry of three years from the passing of the impugned order.

**16\. Accounts.** - (1) If the Commissioner is satisfied that the books of account and other documents maintained by an employer in the normal course of his business are not adequate for verification of the returns filed by the employer under this Act, it shall be lawful for the Commissioner to direct the employer to maintain the books of account or other documents in such manner as he may in writing direct, and thereupon the employer shall maintain such books of account or order documents accordingly.

(2) Where an employer wilfully fails to maintain the books of accounts or other documents as directed under sub-section (1), the Commissioner may, after giving him a reasonable opportunity of being heard, impose a penalty not exceeding rupees five for each day of delay.

**17\. Special mode of recovery.** - (1) Notwithstanding anything contained in any law or contract to the.contrary, the Commissioner may, at any time, or from time to time, by notice in writing, a copy of which shall be forwarded to the assessee at his last address known to the Commissioner, require -

(a) any person from whom any amount of money is due, or may become due, to an assessee on whom notice of demand has been served under this Act, or

(b) any person who holds or may subsequently holds money for or on account of such assessee,

to pay the Commissioner, either forthwith upon the money becoming due or being held or at or within the time specified in the notice (but not before the money becomes due or is held as aforesaid), so much of the money as is sufficient to pay the amount due by the assessee in respect of the arrears of tax, penalty and interest under this Act, or the whole of the money when it is equal to or less than that amount.

_Explanation._\-For the purposes of this section, the amount of money due to an assessee from, or money held for or on account of an assessee by, any person shall be calculated after deducting therefrom such claims (if any) lawfully subsisting, as may have fallen due for payment by such assessee to such person.

(2) The Commissioner may, at any time or from time to time, amend or revoke any such notice, or extend the time for making any payment in pursuance of the notice.

(3) Any person making any payment in compliance with a notice under this section shall be deemed to have made the payment under the authority of the assessee, and the receipt of the Commissioner shall constitute a good and sufficient discharge of the liability of such person, to the extent of the amount referred to in the receipt.

(4) Any person discharging any liability to the assessee after receipt of the notice referred to in this section, shall be personally liable to the Commissioner to the extent of the liability discharged, or the extent of the liability of the assessee for tax, penalty and interest, whichever is less.

(5) Where a person to whom a notice under this section is sent proves to the satisfaction of the Commissioner that the sum demanded or any part thereof is not due to the assessee or that he does not hold any money for on account of the assessee, then nothing contained in this section shall be deemed to require such person to pay any such sum or part thereof, as the case may be, to the Commissioner.

(6) Any amount of money which a person is required to pay to the Commissioner or for which he is personally liable to the Commissioner under this section, shall, if it remains unpaid be recoverable as an arrears of land revenue.

**18\. Production and inspection of accounts and documents and search of premises.** - \[The Commissioner\] may inspect and search any premises, where any profession, trade, calling or employment liable to taxation under this Act is carried on or is suspected to be carried on and may cause production and examination of books, register, accounts or documents relating thereto and may seize such books, registers, accounts or documents as may be necessary:

Provided that, if \[the Commissioner\] removes from the said premises any book, register, account or document, he shall give to the person in charge of the places, a receipt describing the book, register, account or document so removed by him and retain the same only for so long as may be necessary for the purposes of examination thereof or for a prosecution.

**\[19. Refunds of excess payments.** - The prescribed authority shall refund to a \[employer or person\] the amount of tax, penalty, interest and fees (if any) paid by such \[employer or person\] in excess of the amount due from him. The refund may be made either by cash payment or at the option of the \[employer or person\], by deduction of such excess from the amount of tax, penalty, interest and fee due in respect of any other period:

Provided that, the prescribed authority shall first apply such excess towards the recovery of any amount due in respect of which a notice under section 7 has been served, and shall then refund the balance, if any\].

**\[19A. Interest on amount of refund.** - Where, is pursuance of any order under this Act, in respect of any period of assessment commencing on or after the 1st April, 2004, refund of any tax becomes due to an employer or person he shall, subject to the rules, if any, be entitled to receive, in addition to the refund, simple interest at the rate of six per cent per annum for the period commencing on the date next following the last date of the period of assessment to which such order relates and ending on the date of such order or for a period of eighteen months, whichever is less. The interest shall be calculated on the amount of refund due to the employer or any person in respect of the said period after deducting therefrom the amount of penalty and interest, if any, charged in respect of the said period and also the amount of refund, if any, adjusted towards any recovery under this Act. If, as a result of any order passed under this Act, the amount of such refund if enhanced or reduced, as the case may be, such interest shall be enhanced or reduced accordingly.

_Explanation._\- For the purposes of this section, where the refund of tax, whether in full or in part, includes any amount of refund on any payment of tax made after the date prescribed for filing of the last return for the period of assessment, then the interest, in so far as it relates to the refund arising from such payment, shall be calculated from the date of such payment to the date of such order.\]

**\[19B. Interest on delayed refund.** - Where an amount required to be refunded by the Commissioner to any employer or person by virtue of an order issued under this Act is not so refunded to him within ninety days from the date of the order, the State Government shall, pay such employer or person a simple interest at the rate of six per cent. per annum on the said amount from the date immediately following the expiry of the period of ninety days from the date of such order:

Provided that, where the amount becomes refundable by virtue of an order of the Tribunal, the interest under the provisions of this section shall be payable from the date immediately following the expiry of the period of ninety days from the date of receipt of the order of the Tribunal by the Officer whose order forms the subject of the appeal or revision proceedings before the Tribunal.\]

**20\. Offences and penalties.** - Any person or employer who, without sufficient cause, fails to comply with any of the provisions of this Act or the rules framed thereunder shall, on conviction, be punished with fine not exceeding five thousand rupees, and, when the offence is a continuing one, with fine not exceeding fifty rupees per day during the period of the continuance of the offence.

**21\. Offences by companies.** - (1) Where an offence under this Act has been committed by a company, every person who at the time the offence was committed was in charge of and was responsible to the company for the conduct of the business of the company as well as the company, shall be deemed to be guilty of the offence and shall be liable to be proceeded against and punished accordingly:

Provided that, nothing contained in this sub-section shall render any such person liable to any punishment, if he proves that the offence was committed without his knowledge or that he had exercised all the due diligence to prevent the commission of such offence.

(2) Notwithstanding anything contained in sub-section (1), where any offence under this Act has been committed by a company and it is proved that the offence has been committed with the consent or connivance of, or is attributable to any neglect on the part of, any director, manager, secretary or other officer of the company, such director, manager; secretary or other officer shall be deemed to be guilty of that offence and shall be liable to be proceeded against and punished accordingly.

_Explanation._ - For the purposes of this section, -

(a) "company" means any body corporate and includes a firm or other association of individuals; and

(b) "director" in relation to a firm, means a partner in the firm.

**22\. Power to transfer proceedings.** - The Commissioner may, after giving the parties a reasonable opportunity of being heard, wherever it is possible to do so, and after recording his reason for doing so, by order in writing transfer any proceeding or class of proceedings under any provision of this Act, from himself to any other officer, and by may likewise transfer any such proceedings (including a proceeding pending with any officer or already transferred under this section) from any officer to any other officer or to himself:

Provided that, nothing in this section shall be deemed to required any such opportunity to be given where the transfer is from any officer to any other officer and the offices of both are situated in the same city, locality or place.

_Explanation._ - In this section, the word "proceedings" in relation to any assessee whose name is specified in any order issued thereunder means all proceedings under this Act in respect of any year, which may be pending on the date of such order or which may have been completed on or before such date, and includes also all proceedings under this Act which may be commenced after the date of such order in respect of any year in relation to such cases.

**23\. Compounding of offences.** - (1) Subject to such conditions as may be prescribed, the Commissioner may, either before or after the institution of proceedings for an offence under this Act, permit any person charged with the offence to compound the offence on payment of such sum, not exceeding double the amount of tax to which the offence relates, as the Commissioner may determine.

(2) On payment of such sum, as may be determined by the Commissioner under sub-section (1), no further proceedings shall be taken against the person in respect of the same offence.

**24\. Powers to enforce attendance, etc.** - All authorities under this Act shall, for the purposes of this Act, have the same powers as are vested in a Court under the Code of Civil Procedure, 1908 while trying a suit, in respect of enforcing the attendance of and examining any person on oath or affirmation or for compelling the production of any document.

**25\. Bar to proceedings.** - (1) No suit shall lie in any Civil Court to set aside or modify any assessment made or order passed under this Act.

(2) No suit, prosecution, or other legal proceedings shall lie against any authority under this Act or against any employer for anything done or intended to be done in good faith under this Act or the rules framed thereunder.

**26\. Power to delegate.** - The Commissioner may, subject to such conditions and restrictions as the State Government may by general or special order impose, by order in writing delegate to the authorities subordinate to him, either generally or as respects any particular matter or class of matter any of his powers under this Act.

**\[26A. Power to collect statistics.** - (1) The Commissioner may, if he considers that for the purposes of better administration of this Act, it is necessary so to do, he may, by notification in the _Official Gazette_, direct that statistics be collected relating to any matter dealt with, by or in connection with this Act.

(2) Upon such direction being given, the Commissioner or any officer, authorized by the Commissioner in this behalf, may, by notification in the Official Gazette and if found necessary by notice in any newspaper or in such other manner as in the opinion of the Commissioner or the said officer, is best suited to bring the notice to the attention of persons, call upon all persons to furnish such information or returns, as may be specified therein, relating to any matter in respect of which statistics is to be collected. The form in which the officers to whom, such information or returns should be furnished, the particulars which they should contain and the period specified in the notification, within which such information or returns should be furnished, shall be such as may be specified therein.

(3) Any person, who fails to furnish information as provided in this section within the period specified in such notification, shall be liable to pay, by way of penalty, a sum not exceeding rupees ten thousand and in case of continuing default, for a period beyond two months, a further penalty of rupees one hundred for every day of such continuance.\]

**27\. Power to make rules.** - (1) The power to make all rules under this Act shall be exercisable by the State Government by notification in the _Official Gazette_.

(2) Without prejudice to any power to make rules contained elsewhere in this Act, the State Government may make rules consistent with this Act, generally to carry out purposes of this Act and to prescribe fees payable in respect of any applications to be made, forms to be supplied, certificates to be granted and appeals and applications for revision to be made under this Act \[and also any applications for certified copies of documents filed and orders made under this Act.\]

(3) Rules made under this Act shall be subject to the condition of previous publication:

Provided that, if the State Government is satisfied that circumstances exist which render it necessary to take immediate action, it may dispense with the previous publication of any rules to be made under this Act.

(4) Every rule made under this Act shall be laid, as soon as may be after it is made, before each House of the State Legislature while it is in session for a total period of thirty days which may be comprised in one session or in two successive sessions, and if, before the expiry of the session in which it is so laid or the session immediately following, both Houses agree in making any modification in the rule, or both Houses agree that the rule should not be made, and notify such decision in the _Official Gazette,_ the rule shall from the date of publication of such notification have effect only in such modified form or be of no effect, as the case may be, so, however, that any such modification or annulment shall be without prejudice to the validity of anything previously done or omitted to be done under that rule.

**\[27A. Exemptions.** - Nothing contained in section 3 and other provisions of this Act shall apply to,-\]

\[(a) the members of the Forces as defined in the Army Act, 1950 or the Air Force Act, 1950 and the members of Indian Navy as defined in the Navy Act, 1957 serving in any part of the State and drawing pay and allowances as Army or Air Force or Navy, as the case may be, including the members of auxiliary forces or reservists, or reserve and auxiliary services serving in any part of the State and drawing pay and allowances as such auxiliary forces or reservists, or reserve and auxiliary services, as the case may be, under the budgetary allocations of the Defence Services;\]

\[_Explanation._ - It is hereby declared for the removal of doubts that nothing in this clause shall apply or shall be taken to have applied during any period starting on or after the 1st May, 2000 to persons drawing pay and allowances from the establishments of the Defence Ordinance Factories situated in any part of the State.\]

\[(a-1)\] \[\* \* \*\].

(b) the _badli_ workers in the textile industry.

\[(c) Any person suffering from a permanent physical disability (including blindness), being a permanent physical disability specified in the rules made in this behalf by the State Government, which is certified by a physician, a surgeon or an oculist, as the case may be, working in a Government Hospital and which has the effect of reducing considerably such individual's capacity for normal work or engaging in a gainful employment or occupations:

Provided that such individual or, as the case may be, employer produces the aforesaid certificate before the prescribed authority in respect of the first assessment year for which he claims deduction under this sub-section :

Provided further that the requirement of producing the certificate from a physician, a surgeon or an oculist, as the case may be, working in a Government Hospital shall not apply to an individual who has already produced a certificate before the prescribed authority under the provisions of this sub-section as they stood immediately before the 1st day of April 1995.

_Explanation._ - For the purpose of this sub-section, the expression "Government Hospital" includes a departmental dispensary whether full time or part time established and run by a Department of the Government for the medical attendance and treatment of a class or classes of Government servants and members of their families, a hospital maintained by a local authority and any other hospital with which arrangements have been made by the Government for the treatment of Government servants;\]

\[(d) women exclusively engaged as agents under the Mahila Pradhan Kshetriya Bachat Yojana of Directorate of Small Savings.\];

\[(e) Parents or Guardian of any person who is suffering from mental retardation specified in the rules made in this behalf, which is certified by a psychiatrist working in a Government Hospital :

Provided that such individual produces such certificate before the prescribed authority in respect of the first, assessment year for which he claims deduction under this sub-section.

_Explanation._ - For the purpose of this clause, the expression "Government Hospital" will have the same meaning as assigned to it in clause (c).;

(f) the persons who have completed the age of sixty-five years.\]

\[(g) parents of guardians of a child suffering from a physical disability as specified in clause (c);\].

**28\. Amendment of certain enactments.** - (1) The enactments specified in the second column of Schedule II are hereby amended in the manner and to the extent specified in the third column thereof :

Provided that, nothing in the said amendments shall affect or be deemed to affect,-

(i) any right, obligation or liability already acquired, accrued or incurred for anything done or suffered, in respect of any period preceding the date of coming into force of these amendments;

(ii) any legal proceeding or remedy whether initiated or availed of before or after the date of coming into force of these amendments, in respect of any such right, obligation or liability.

(2) The levy, assessment or recovery of any tax or the imposition or recovery of and penalty, in respect of such period, under the provisions of the relevant enactments and all proceedings under them, in respect of all matters aforesaid, shall be initiated and disposed of, or continued and disposed of, as the case may be, as if this Act had not been enacted.

**29\. Grants to local authorities for loss of revenue.** - Out of the proceeds of the tax and penalties and interest and fees recovered under this Act there shall, under appropriation duty made by law, be paid annually to such local authorities as were levying a tax on professions, trades, callings and employments immediately before the commencement of this Act, and whose power to levy such tax has been withdrawn under the provisions of this Act, such amounts on the basis of the highest collection made by them in any year during the period of three years immediately preceding the commencement of this Act, as may be determined by the State Government in this behalf.

**30\. \[Amounts to be paid into the Fund established under the Maharashtra Employment Guarantee Act, 1977.** - The proceeds of the tax levied and collected under this Act, together with penalties and interest and fees recovered thereunder, shall first be credited to the Consolidated Fund of the State, and after deducting the expenses of collection and recovery as determined by the State Government and the amounts of grants made to the local authorities under section 29, out of the remaining amount, the amount necessary to ensure that, at the beginning of every Financial Year, the amount standing to the credit of the Fund 1977, is not less than Rupees 2,000 Crore, shall under the appropriation duly made by law in this behalf, be entered into, and transferred to the Fund established under that Act.\]

\[Schedule - I\]

(_See_ section 3)

_**w.e.f. 1-7-2009 to date**_

**Schedule of Rates of Tax on Professions, Trades, Callings and Employments**

   

**Serial No.  
1**

**Class of persons  
  
2**

**Rate of tax Rs.  
  
3**

**w.e.f.  
  
4**

\[1.

Salary and wage earners - such persons whose monthly salaries or wages, -

(a) Do not exceeds Rs. 5000

Nil

1-7-2009 to date

(b) Exceeds Rs. 5000 but do not exceed Rs. 10,000

175 per month.

1-7-2009 to date

(c) Exceeds Rs. 10,000

2,500 per annum, to be paid in the following manner:-

(a) rupees two hundred per month except for the month of February;

(b) rupees three hundred for the month of February.\]

1-7-2009 to date

\[1-A.

Persons as notified under section 4B.

2,500 per annum.\]

2.

(a) Legal Practitioners including Solicitor and Notaries;

  

  

(b) Medical Practitioners including Medical Consultants and Dentists;

(c) Technical and Professional Consultants, including Architects, Engineers, R.C.C. Consultants, Tax Consultants, Chartered Accountants, Actuaries and Management Consultants;

(d) Chief Agents, Principal Agents, Insurance Agents and Surveyors and Loss Assessors registered or licensed under the Insurance Act, 1938, (4 of 1938), U.T.I. Agents Under U.T.I. Scheme, N.S.S. Agents under postal scheme;

(e) Commission Agents, Dalals and Brokers (other than estate brokers covered by any other entry elsewhere in this Schedule);

(f) All types of Contracts (other than building contractors covered by any other entry elsewhere in this Schedule); and

(g) Diamond dressers and diamond polishers, having not less than one year's standing in the profession.

2,500 per annum.

1-4-2006 to date

3.

(a) Members of Association recognised under the Forward Contracts (Regulation) Act, 1952; (74 of 1952)

2,500 per annum.

1-4-2006 to date

(b) (i) Member of Stock Exchanges recognised under the security Contracts (Regulation) Act, 1956; (42 of 1956)

2,500 per annum.

1-4-2006 to date

(ii) Remisiers recognised by the Stock Exchange;

2,500 per annum.

1-4-2006 to date

4.

(a) Building Contractors;

2,500 per annum.

1-4-2006 to date

(b) Estate Agents, Brokers or Plumbers, having not less than one year's standing in the profession.

2,500 per annum.

1-4-2006 to date

5.

Directors (other than those nominated by Government) of Companies registered under the Companies Act, 1956, (1 of 1956) and Banking Companies as defined in the Banking Regulation Act, 1949 (10 of 1949).

_Explanation._ - The term 'Directors' for the purpose of this entry will not include the persons who are Directors of the companies whose registered offices are situated outside the State of Maharashtra and who are not residing in the state of Maharashtra.

2,500 per annum.

1-4-2006 to date

6.

(a) Bookmakers and Trainers licensed by the Royal Western India Turf Club Limited;

2,500 per annum.

1-4-2006 per date

(b) Jockeys licensed by the said Club

2,500 per annum.

1-4-2006 to date

7.

Self-employed persons in the Motion Picture Industry, Theatre, Orchestra, Television, Modelling or Advertising Industries as follows:-

  

  

(a) Writers, Lyricists, Directors, Actors and Actresses (excluding junior Artists), Musicians, Playback Singers, Cameramen, Recordists, Editors and Still-Photographers;

2,500 per annum.

1-4-2006 to date

(b) Junior Artists, Production Managers, Assistant Directors, Assistant Recordists, Editors and Dancers.

1,000 per annum.

1-4-2006 to date

8.

Dealers registered under the Maharashtra Value Added Tax Act, 2002, (Maharashtra IX of 2005) or Dealers registered only under the Central Sales Tax Act, 1956, (74 of 1956) whose annual turnover of sales or purchases,-

  

(i) is rupees 25 lakhs or less

2,000 per annum.

1-4-2006 to date

(ii) exceeds rupees 25 lakhs

2,500 per annum

1-4-2006 to date

9.

Occupiers of Factories as defined in the Factories Act, 1948 (63 of 1948), who are not covered by entry 8 above.

2,500 per annum.

1-4-2006 to date

10.

(1) (A) Employers of establishments as defined in the Bombay Shops and Establishments Act, 1948, where their establishments are situated within an area to which the aforesaid Act applies, and who are not covered by entry 8 -

  

  

Such employers of establishment,-

  

  

(a) where no employee is employed

1,000 per annum.

1-4-2006 to date

(b) where not exceeding two employees are employed;

2,000 per annum.

1-4-2006 to date

(c) where more than two employees are employed.

2,500 per annum.

1-4-2006 to date

(B) Employers of establishments as defined in the Bombay Shops and Establishments Act, 1948, (Bombay LXXIX of 1948) where their establishments are not situated within an area to which the aforesaid Act applies, and who are not covered by entry 8.

  

  

Such employers of establishment,-

  

  

(a) where no employee is employed;

500 per annum.

1-4-2006 to date

(b) where not exceeding two employees are employed;

1,000 per annum.

1-4-2006 to date

(c) where more than two employees are employed

2,500 per annum.

1-4-2006 to date

(2) Persons owning/running STD/ISD booths or Cyber Cafes, other than those owned or run by Government or by physically handicapped persons;

1,000 per annum.

1-4-2006 to date

(3) Conductors of Video or Audio Parlours, Video or Audio Cassette Libraries, Video Game Parlours;

2,500 per annum.

1-4-2006 to date

(4) Cable Operators, Film Distributors

2,500 per annum.

1-4-2006 to date

(5) Persons owning/running marriage halls, conference halls, beauty parlours, health centres, pool parlours;

2,500 per annum.

1-4-2006 to date

(6) Persons running/conducting coaching classes of all types.

2,500 per annum.

1-4-2006 to date

11.

Owners or Lessees of Petrol/Diesel/Oil Pumps and Service Stations/Garages and workshops of Automobiles.

2,500 per annum.

1-4-2006 to date

12.

Licensed Foreign Liquor Vendors and employers of Residential Hotels and Theatres as defined in the Bombay Shops and Establishments Act, 1948. (Bombay LXXIX of 1948)

2,500 per annum.

1-4-2006 to date

13.

Holders of permits for Transport Vehicles granted under the Motor Vehicles Act, 1968, (59 of 1988) which are used or adopted to be used for hire or reward, where any such person holds permit or permits for, -

  

  

(a) three wheeler goods vehicles, for each such vehicle;

750 per annum.

1-4-2006 to date

(b) any taxi, passenger car, for each such vehicle;

1,000 per annum.

1-4-2006 to date

(c) (i) goods vehicles other than those covered by (a);

1,500 per annum.

1-4-2006 to date

(ii) trucks or buses for each such vehicle:

1,500 per annum.

1-4-2006 to date

Provided that the total tax payable by a holder under this entry shall not exceed rupees, 2,500 per annum.

  

  

14.

Money-lenders licensed under the Bombay Money lenders Act, 1946 (Bombay XXXI of 1947)

2,500 per annum.

1-4-2006 to date

15.

Individuals or Institutions conducting Chit Funds.

2,500 per annum.

1-4-2006 to date

16.

Co-operative Societies registered or deemed to be registered under the Maharashtra Co-operative Societies Act, 1960 (Maharashtra XXIV of 1961) and engaged in any profession, trade or calling,-

  

  

(i) State level Societies

2,500 per annum.

1-4-2006 to date

(ii) Co-operative sugar factories and Spinning Mills

2,500 per annum.

1-4-2006 to date

(iii) District Level Societies

750 per annum.

1-4-2006 to date

(iv) Handloom Weavers Co-operative Societies

500 per annum.

1-4-2006 to date

  

(v) All other Co-operative Societies not covered by clauses (i), (ii), (iii) and (iv) above

750 per annum.

1-4-2006 to date

17.

Banking Companies, as defined in the Banking Regulation Act, 1949, (10 of 1949)

2,500 per annum.

1-4-2006 to date

18.

Companies registered under the Companies Act, 1956 (1 of 1956) and engaged in any profession, trade or calling.

2,500 per annum.

1-4-2006 to date

\[18A.

Limited liability partnership, registered under the Limited Liability Partnership Act, 2008, (6 of 2009).

2,500 per annum.

31.3.2018 to date\]

\[19.

(a) Each partner of a firm (whether registered or not under the Indian Partnership Act, 1932), (9 of 1932)

2,500 per annum.

31.3.2018 to date

  

(b) Each partner of a limited liability partnership, registered under the Limited Liability Partnership Act, 2008, (6 of 2009) engaged in any profession, trade or calling.

2,500 per annum.

31.8.2018 to date\]

20.

Each Co-parcener (not being a minor) of a Hindu Undivided Family, which is engaged in any profession, trade or calling.

2,500 per annum.

1-4-2006 to date

\[20-A.

Person, registered under the Maharashtra Goods and Services Tax Act, 2017.

2,500 per annum.\]

21.

Persons other than those mentioned in any of the preceding entries who are engaged in any profession, trade, calling or employment and in respect of whom a notification is issued under the second proviso to sub-section (2) of section 3.

2,500 per annum.

1-4-2006 to date

**Note 1** - Notwithstanding anything contained in this Schedule, where a person is covered by more than one entry of this Schedule, the highest rate of tax specified under any of those entries shall be applicable in his case. This provision shall not be applicable to entry 16(iv) of the Schedule.

**Note 2** - For the purposes of Entry 8 of the Schedule, the Profession Tax shall be calculated on the basis of the "turnover of sales or purchases" of the previous year. If there is no previous year for such dealer, the rate of Profession Tax shall be Rs. 2,000. The expressions "turnover of sales" or "turnover of purchases" shall have the same meaning as assigned to them, respectively, under the Maharashtra Value Added Tax Act, 2002". (Maharashtra IX of 2005).

**Notifications**

**G. N., F. D., No. PFT 1275-(ii) A-6, dated 12th June, 1975 (M.G., Part IV-B, page 623) G. N., F. D., No. PTA 1896/CR-64/Taxation-1, dated 3rd July, 1996 (M.G., Part IV-B, page 884)** - In exercise of the powers conferred by sub-section (I) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975), the Government of Maharashtra, hereby appoints all officers for the time being appointed as-

(i) the Additional Commissioner.

(ii) the Deputy Commissioners of Sales Tax.

(iiA) the Senior Assistant Commissioners of Sales Tax.

(iii) the Assistant Commissioners of Sales Tax, and

(iv) the Sales Tax Officers.

under the Bombay Sales Tax Act, 1959 (Bombay LI of 1959), also to be -

(i) the Additional Commissioner of Profession Tax.

(ii) the Deputy Commissioners of Profession Tax.

(iiA) the Senior Assistant Commissioners of Profession Tax.

(iii) the Assistant Commissioners of Profession Tax, and

(iv) the Profession Tax Officers respectively.

under the Maharashtra Sate Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975).

2\. The Government of Maharashtra further specifies that the area which may be for the time being within the jurisdiction of each of these officers under the Bombay Sales Tax Act, 1959 (Bombay LI of 1959), shall also be the area within his jurisdiction under the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975).

**G. N., F. D No. PFT. 1275-(i) A-6, dated 12th June, 1975 (M. G., Part IV-8, page 623)** - In exercise of the powers conferred by clause (a) of sub-section (I) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975), the Government of Maharashtra hereby appoints the officer for the time being appointed as the Commissioner of Sales Tax under the Bombay Sales Tax Act, 1959 (Bombay LI of 1959), also to be the Commissioner of Profession Tax for the whole of the State, under the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975).

**G.N.F.D. No. PFT 1105/CR 122-A Taxation-3, dated the 3rd November, 2006 (M.G. Part IV-B. Ext. page 1818)** - In exercise of the powers conferred by clause (a) of sub-section (1) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975), the Government of Maharashtra hereby appoints the Transport Commissioner, Regional Transport Officers, Deputy Regional Transport Officers and Assistant Regional Transport Officers, appointed by it under Motor Vehicles Act, 1988 (Act No. 59 of 1988), to be the Additional Commissioner of Profession Tax, Joint Commissioners of Profession Tax, Deputy Commissioners of Profession Tax and Profession Tax Officers, respectively, for carrying out the purposes of the said Act.

**G. N., F. D., No. STA. 1080/193/80-ADM-6, dated 22nd October, 1980 (M.G., Part IV-B, page 978)** - In exercise of the powers conferred by clause (b) of sub-section (I) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI 1975), read with Government Notification, Finance Department, No. PFT. 1275/(ii)/A-6, dated the 12th June 1975, the Government of Maharashtra hereby directs that the officers mentioned in column 2 of the Schedule hereto shall have jurisdiction for exercising the powers and performing the duties under the said Act, over the areas respectively specified against them in column 3 of the said schedule.

Schedule

  

_Serial  
No._

_Officers with their designation_

_Areas_

_1._

_2._

_3._

1.

Deputy Commissioner of Profession Tax, Maharashtra State, Bombay.

Maharashtra State.

2.

Assistant Commissioner of Profession Tax-I, Bombay City  
Division, Bombay.

Limits of Greater Bombay.

3.

Assistant Commissioner of Profession Tax-II, Bombay City  
Division, Bombay.

Do.

\[4.

Assistant Commissioner of Sales Tax (Profession Tax)-III,  
Bombay City Division, Bombay.

Maharashtra State\]

**G. N., F. D., No. JUR. 1282/215/ADM-6, dated 14th January, 1983 (M.G., Part IV-B, page 82)** - In exercise of the powers conferred by clause (b) of sub-section (1) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Maharashtra XVI of 1975) (hereinafter referred to as "the said Act") read with Government Notification, Finance Department No. PFT-1275-(II)-A-6, dated the 12th June, 1975 and in supersession of Government Notification, Finance Department, No. STA.1080/194/801ADM-6, dated the 30th September, 1980 read with Government Notification, Finance Department, No. STA. 1080/194/80/ADM-6, dated 14th January, 1982 the Government of Maharashtra hereby specifies the areas mentioned in column 3 of the Schedule hereto be the local areas over which the officers mentioned in column 2 of the said Schedule shall have jurisdiction for exercising the powers and performing the duties under the said Act.

Schedule

  

_Serial No._

_Officers_

_Areas_

_1_

_2_

_3_

1.

Profession Tax Officer, Bombay

Limits of Greater Bombay

2.

Profession Tax Officer, Thane

Talukas of Thane and Bhiwandi of revenue district of Thane.

3.

Profession Tax Officer, Kalyan

Talukas of Kalyan, Ulhasnagar, Shahapur and Murbad revenue district of Thane.

4.

Profession Tax Officer, Raigad

Revenue district of Raigad.

5.

Profession Tax Officer, Nashik

Revenue district of Nashik, except talukas of Malegaon, Yeola, Nandgaon, Chandor, Kalyan and Baglan

6.

Profession Tax Officer, Malegaon

Taluka of Malegaon, Yeola, Nandgaon, Chandor, Kalyan and Baglan of revenue district of Nashik.

7.

Profession Tax Officer, Jalgaon

Revenue district of Jalgaon.

8.

Profession Tax Officer, Dhule

Revenue district of Dhuie.

9.

Profession Tax Officer, Ahmadnagar.

Revenue district of Ahmadnagar.

10.

Profession Tax Officer, Pune

Revenue district of Pune.

11.

Profession Tax Officer, Solapur

Revenue district of Solapur.

12.

Profession Tax Officer, Sangli

Revenue district of Sangli

13.

Profession Tax Officer, Satara

Revenue district of Satara

14.

Profession Tax Officer, Ratnagiri

Revenue district of Ratnagiri & Sindhudurg.

15.

Profession Tax Officer, Kolhapur

Revenue district of Kolhapur.

16.

Profession Tax Officer, Jalna

Revenue district of Jalna.

17.

Profession Tax Officer, Nagpur

Revenue district of Nagpur.

18.

Profession Tax Officer, Amravati.

Revenue district of Amravati.

19.

Profession Tax Officer, Akola.

Revenue district of Akola.

20.

Profession Tax Officer, Aurangabad

Revenue district of Aurangabad.

21.

Profession Tax Officer, Parbhani

Revenue district of Parbhani.

22.

Profession Tax Officer, Beed.

Revenue district of Beed.

23.

Profession Tax Officer, Nanded.

Revenue district of Nanded.

24.

Profession Tax Officer, Latur

Revenue district of Osmanabad.

25.

Profession Tax Officer, Kharngaon.

Revenue district of Buldhana.

26.

Profession Tax Officer, Yeotmal

Revenue district of Yeotmal.

27.

Profession Tax Officer, Gondia

Revenue district of Bhandara.

28.

Profession Tax Officer, Chandrapur

Revenue district of Chandrapur.

29.

Profession Tax Officer, Wardha

Revenue district of Wardha.

**G.N. F.D. No. PFT.1105/C.R.-75-B/Taxation-3, dated the 13th January, 2007** - In exercise of the powers conferred by sub-section (1) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975, (Maharashtra XVI of 1975) (hereinafter referred to as "the said Act") and in supersession of all notifications or orders issued in this behalf, the Government of Maharashtra hereby appoints the Commissioner of Sales Tax, the Additional Commissioner of Sales Tax, the Joint Commissioner of Sales Tax, the Deputy Commissioner of Sales Tax, the Assistant Commissioner of Sales Tax and the Sales Tax Officer, appointed under the Maharashtra Value Added Tax Act, 2002 (Maharashtra IX of 2005) to be the Commissioner of Profession Tax, the Additional Commissioner of Profession Tax, the Joint Commissioner of Profession Tax, the Deputy Commissioner of Profession Tax, the Assistant Commissioner of Profession Tax and the Profession Tax Officer respectively and all officers appointed immediately before the commencement of the Maharashtra Tax Laws (Levy, Amendment and Validation) Act, 2006, as the Deputy Commissioner of Profession Tax, the Assistant Commissioner of Profession Tax and the Profession Tax Officer to be the Joint Commissioner of Profession Tax, the Deputy Commissioner of Profession Tax, and the Profession Tax Officer, respectively for the purposes of the said Act.

**G.N. F.D. No. PFT.1105/C.R.-75-B/Taxation-3, dated the 11th January, 2007** - In exercise of the powers conferred by sub-section (1) of section 12 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975, (Maharashtra XVI of 1975) (hereinafter referred to as "the said Act") and in supersession of all notifications or orders issued in this behalf, the Government of Maharashtra hereby appoints the Commissioner of Sales Tax, the Additional Commissioner of Sales Tax, the Joint Commissioner of Sales Tax, the Deputy Commissioner of Sales Tax, the Assistant Commissioner of Sales Tax and the Sales Tax Officer, appointed under the Maharashtra Value Added Tax Act, 2002 (Maharashtra IX of 2005) to be the Commissioner of Profession lax, the Additional Commissioner of Profession Tax, the Joint Commissioner of Profession Tax, the Deputy Commissioner of Profession Tax, the Assistant Commissioner of Profession Tax and the Profession Tax Officer respectively and all officers appointed immediately before the commencement of the Maharashtra Tax Laws (Levy, Amendment and Validation) Act, 2006, as the Deputy Commissioner of Profession Tax, the Assistant Commissioner of Profession Tax and the Profession Tax Officer to be the Joint Commissioner of Profession Tax, the Deputy Commissioner of Profession Tax, and the Profession Tax Officer, respectively for the purposes of the said Act.

**G.N. F.D. No. PFT.1105/C.R.-75-A/Taxation-3, dated the 11th January, 2007** - In exercise of the powers conferred by paragraph (B) of sub-clause (b) of clause (7) of rule 2 of the Maharashtra State Tax on Professions, Trades, Callings and Employments Rules, 1975, the Government of Maharashtra hereby notifies the following branches of the State Bank of India or of any subsidiary branch as defined in the State Bank of India Act, 1959 (38 of 1959) and of a corresponding new bank constituted under the Banking Companies (Acquisition and Transfer of Undertaking) Act, 1970 (5 of 1970) for the purposes of paragraph (B) of sub-clause (b) of clause (7) of rule 2 of the said rules:-

   

_**Sr. No.**_

_**Place**_

_**District**_

_**Name of the Bank**_

_**(1)**_

_**(2)**_

_**(3)**_

_**(4)**_

1.

Khopoli

Raigad

Bank of India.

2.

Taloja

Raigad

Bank of India.

3.

Kalyan

Thane

Bank of Maharashtra, Bank of India, Bank of Baroda, Dena Bank, Central Bank of India.

4.

Dombivali

Thane

Bank of Maharashtra, Dena Bank, Central Bank, Bank of Baroda.

5.

Ulhasnagar (Camp No. 1 to 5)

Thane

Syndicate Bank, Bank of India, Canara Bank, Central Bank of India, Bank of Baroda, Dena Bank, Bank of Maharashtra, Punjab National Bank.

6.

Bhiwandi

Thane

Bank of India, Bank of Maharashtra, Central Bank of India.

7.

Murbad

Thane

Bank of Maharashtra.

8.

Shahapur

Thane

Bank of Maharashtra.

9.

Badlapur

Thane

State Bank of India, M.I.D.C. Kulgaon.

10.

Bhainder

Thane

Dena Bank, Bank of Maharashtra, State Bank of India.

11.

Tarapur

Thane

Bank of India, Mahendra Park, Bhoisar, Bank of Maharashtra, State Bank of India.

12.

Ballarpur

Chandrapur

State Bank of India.

13.

Gadchandur

Chandrapur

State Bank of India.

14.

Sangli

Sangli

Bank of Maharashtra, Kirloskar Wadi.

15.

Lote Parshuram Industrial Area

Ratnagiri

Bank of India.

16.

Valunj, MIDC

Aurangabad

Bank of Maharashtra.

**Order**

**(Under Section 9(2) of the Central Sales Tax Act, 1956, read with section 70 of the Bombay Sales Tax Act, 1959)**

**Notification No. Sr. DC(A&R)/TAP/67/Adm-3, Date : 17-7-2007**

In view of the introduction of the Maharashtra Value Added Tax Act, 2002 \[MVAT Act\] with effect from the 1st April, 2005 in the State of Maharashtra and the repeal of the earlier laws specified in the MVAT Act, the organizational set-up of the Sales Tax Department is being restructured. The restructuring will go on throughout the transition period till the work related to the Bombay Sales Tax Act, 1959 and the earlier laws, is substantially over and may in the exigencies of the circumstances continue for some time thereafter. The exercise of restructuring involves _inter-alia_, abolition, merger and consolidation of posts creation of new posts, creation of new administrative units including divisions and zones, designating the new posts and re-designating the old posts.

In view of such considerations, nine different office orders have been issued so far. The dates and numbers of these orders are given at the top of this order. For the purpose of accessibility and bringing these orders to the notice of the departmental authorities as well as the trading community, the texts of these orders are hosted on the site http://vat.maharashtra.gov.in. These orders provide for the merger of the posts, creation of posts, re-designations and designations of the posts etc. The sales tax authorities administer several different Acts. The present order is in respect of proceedings under the Central Sales Tax Act, 1956 and the cases in so far as periods ending on or before the 31st March, 2005 are concerned. It has now become necessary to reallocate amongst the officers the cases as well as the proceedings including pending proceedings in view of the changes brought about by the aforesaid orders.

I, Sanjay Bhatia, Commissioner of Sales Tax, Maharashtra State, in exercise of the powers granted to me by sub-section (2) of section 9 of the Central Sales Tax Act, 1956 read with section 70 of the Bombay Safes Tax Act, 1959, do accordingly, in respect of periods ending or or before 31st March, 2005, assign by transfer cases under the Central Sales Tax Act, 1956 and transfer proceedings including pending proceedings under the said Act.

\- from the officers appointed on different posts to whom the cases and proceedings including pending proceedings stood allotted as per their posts on the 31st March, 2007,

\- to the officers with whose post their posts stand merged, or to the corresponding officers holding the newly created posts, as the case may be, whether or not the posts are temporary.

\- in so far as the offices of both the officers from whom the cases or proceedings are to be transferred and the officers to whom the cases or proceedings are to be transferred are situated in the same district as constituted under section 4 of the Maharashtra Land Revenue Code, 1960 or, as the case may be, in Greater Mumbai.

The reference in this order to restructuring, abollion, merger and consolidation of posts, creation of posts, divisions and zones, designation and re-designation of posts and corresponding officer holding the newt/created posts are to be read in me context of the office orders specified at the top of this order. It is needless to add that this order takes effect from the respective dates from which the said office order takes effect.