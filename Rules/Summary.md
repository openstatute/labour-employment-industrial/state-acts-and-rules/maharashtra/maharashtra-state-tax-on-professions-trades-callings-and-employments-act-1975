      Profession Tax Act, 1975 � Maharashtra  

         ![](../images/header1.jpg)

<!-- MSFPhover = (((navigator.appName == "Netscape") && (parseInt(navigator.appVersion) >= 3 )) || ((navigator.appName == "Microsoft Internet Explorer") && (parseInt(navigator.appVersion) >= 4 ))); function MSFPpreload(img) { var a=new Image(); a.src=img; return a; } // --><!-- if(MSFPhover) { MSFPnav1n=MSFPpreload("../\_derived/back\_cmp\_Profile110\_back.gif"); MSFPnav1h=MSFPpreload("../\_derived/back\_cmp\_Profile110\_back\_a.gif"); } // -->[![Back](../_derived/back_cmp_Profile110_back.gif)](customs-act-1962.html) <!-- if(MSFPhover) { MSFPnav2n=MSFPpreload("../\_derived/home\_cmp\_Profile110\_home.gif"); MSFPnav2h=MSFPpreload("../\_derived/home\_cmp\_Profile110\_home\_a.gif"); } // -->[![Home](../_derived/home_cmp_Profile110_home.gif)](../index.htm) <!-- if(MSFPhover) { MSFPnav3n=MSFPpreload("../\_derived/up\_cmp\_Profile110\_up.gif"); MSFPnav3h=MSFPpreload("../\_derived/up\_cmp\_Profile110\_up\_a.gif"); } // -->[![Up](../_derived/up_cmp_Profile110_up.gif)](index-part4.html)

https://wirc-icai.org/wirc-reference-manual/part4/profession-tax-act-1975-maharashtra.html

Reference Manual 2022-2023

**PROFESSION TAX ACT, 1975 — MAHARASHTRA**
==========================================

**I. PURPOSE AND SCOPE**

The Maharashtra State Tax on Professions, Trades, Callings and Employments Act, 1975 (Profession Tax Act) applies to whole of State of Maharashtra and has come into operation from 1-4-1975. The Tax is levied and collected subject to the provisions of Article 276 of the Constitution of India for the benefit of the State.

 **II.** **LEVY OF TAX-ENROLLED PERSONS \[PTEC\]**

Every Person including LLP registered under the LLP Act, 2008, excluding Partnership Firm whether registered or not under Indian Partnership Act, 1932 and HUF, engaged actively or otherwise in any profession, trade, calling or employment and covered by Schedule I appended to the Profession Tax Act, 1975 (given separately herein below) is liable to pay Tax at the rate mentioned against each entry in Schedule I.  
The Tax payable by one person will not exceed Rs.2,500/- for one year. In the case of person liable to enroll but remained un-enrolled, the liability to pay tax in respect of un-enrolled period will not exceed-

1.  4 years where Enrolment Certificate is granted on or after 1-4-2017;
2.  Where an application for enrolment is filed between 1-4-2016 and 30-9-2016 or is pending on 1-4-2016, the liability shall not be for any period prior to 1-4-2013;
3.  8 years in any other case from the end of the year immediately preceding the year in which Enrolment Certificate is granted or proceeding for enrolment has been initiated, whichever is earlier.

Schedule I appended to the Profession Tax Act, 1975 contains 21 entries and the rates of taxes are mentioned against each entry. Where a person is covered by more than one entry of this Schedule, the highest rate of tax specified under any of those entries shall be applicable in his case. 

**III. EXEMPTIONS**

Section 27A provides for exemptions from payment of profession tax to -

1.  Persons who have completed the age of 65 years.
2.  Person suffering from permanent physical disability including blindness specified in the rule 32 and Parents or Guardian of child.
3.  Parents or Guardian of a child suffering from permanent physical disability including blindness specified in the rule 32.
4.  Person with Intellectual and Development Disabilities (Mental Retardation) specified in the rule 32 and Parents or Guardian of such person.
5.  Badli workers in Textile Industry.
6.  Women exclusively engaged as agents under the Mahila Pradhan Kshetriya Bachat Yojana of Directorate of Small Savings.
7.  Members of the forces as defined in the Army Act,1950 or the Air Force Act, 1950 and Members of Indian Navy as defined in the Navy Act,1957 including members of auxiliary forces or reservists, serving in the state excluding persons drawing pay and allowances from the establishments of the Defence Ordnance Factories.
8.  Armed members of the Central Reserve Police Force and armed members of the Border Security Force. 

**IV. REGISTRATION AND PAYMENT OF TAXES BY ENROLLED PERSONS**

The person liable to pay Profession Tax (other than a person earning salary or wages, in respect of whom the tax is payable by his employer) has to apply within 30 days from date of liability to obtain Enrollment Certificate.  
  
Where a person is a citizen of India and is in employment of any diplomatic or consular office or trade commissioner of any foreign country situated in any part of the State, such person, if liable to pay tax, shall obtain a certificate of enrolment and pay the tax himself.  
  
Similarly, where a person earning a salary or wage, is also covered by one or more entries other than entry 1 in Schedule I and the rate of tax under any such other entry is more than the rate of tax under entry 1 in that Schedule, or is simultaneously engaged in employment of more than one employer, shall obtain a certificate of enrolment and pay the tax himself. In such case, such person should furnishes to his employer or employers a certificate in form II-B or II-C declaring that he shall get enrolled and pay the tax himself, then the employer or employers of such person shall not deduct the tax from the salary or wage payable to such person and such employer or employers, as the case may be, shall not be liable to pay tax on behalf of such person.  
  
If the tax is revised and the person becomes liable to pay tax at a rate higher or lower than the one mentioned in his certificate of enrolment, he has to apply for a revised certificate of enrolment within 30 days from the date of revised liability.  
  
The Application for new Registration or amendment needs to be made in Form II and required to be filed electronically on Department website. Where an applicant has more than one place of work within the State of Maharashtra, he shall make a single application in respect of all such places, name in such application one of such places as the principal place of work and submit such application to the prescribed authority in whose jurisdiction the said principal place of work is situated.  
  
An applicant having more than one place of work in the State of Maharashtra shall be granted only one certificate of enrolment, however, as many copies of the certificate shall be issued to him as there are additional places of work, in respect of the places of work other than the principal place of work.  
  
The holder of the certificate of enrollment, shall display conspicuously at his place of work the certificate of enrollment or a copy thereof.  
  
From 6-3-2019, the date upto which tax is required to be paid is changed to **31st March** of respective financial year from **30th June** of respective financial year.  
  
The payment is to be made in MTR-6 challan by selecting Form ID-VIII in the challan, by electronic mode only. 

**V. COMPOSITION SCHEME (One Time Payment of Tax (OTPT) Scheme)**

1.  From 01.04.2019, new OTPT scheme is offered for payment of profession tax in advance for any number of years subject to minimum 3 years and maximum for 35 years.
2.  An enrolled person opting for the Scheme is required to make payment as per Table given in the Notification.
3.  Enrolled person may opt for the Scheme again after earlier period under the scheme is over. However, the benefit can be availed at a time for a minimum period of three years upto a maximum period of thirty five years for those who is liable to pay tax @2,500/- per annum and upto a maximum period of five years for other persons.
4.  Amount payable as per the OTPT scheme will have to be paid electronically.
5.  Enrolled person who has discharged his liability for payment of tax for a total continuous period of five years by making payment in advance under earlier scheme prior to 1st April 2018 may also opt for the Scheme after completion of such period of five years.
6.  The enrolled person who has already paid Profession Tax for the year 2018-2019 or has paid any lump-sum amount on or after 1st April 2018 for the periods starting from 1st April 2018, can also avail the benefit of Scheme by paying the balance amount payable for the period opted under the scheme as per the Table.
7.  If the enrolled person, who has availed the benefit of Scheme and has discharged his liability of Profession tax for a particular period, joins any employment during the period covered under the Scheme then, such person shall furnish to the employer ‘One Time Profession Tax Payment Certificate’ in Form A appended to this Scheme. In such case his liability to pay profession tax shall be restricted to the amount paid under the Scheme and the employer shall not be liable to deduct Profession Tax of the said person until completion of his period under Scheme.
8.  If the enrolled person has paid the Profession tax under the Scheme for a particular period and subsequently he is covered by any other entry having higher rate of tax than the rate applicable at the time of opting the scheme then, his liability to pay tax shall not be varied due to such change in the entry under Schedule I.
9.  Once the amount is paid under the Scheme, no refund of the amount paid shall be granted under any Circumstances.
10.  Person making payment under OTPT Scheme will get certificate in Form A containing the periods for which payment has been made under the Scheme.
11.  Earlier 5 years composition Scheme, which was bought into effect from 01.05.1993 is discontinued from 01.04.2018.

 **VI. EMPLOYER’S LIABILITY TO DEDUCT AND PAY — REGISTRATION — PTRC**

The Tax payable by person earning salary or wages is to be deducted by his employer from the salary or wages payable to him, as per slab rates applicable to them as mentioned in Entry 1 in the Schedule I and shall be paid by employer, irrespective of whether such deduction has been made or not, on behalf of all such persons.  
  
If the employee is enrolled as a person and such person furnishes to his employer or employers a certificate in form II-B or II-C declaring that he shall get enrolled and pay the tax himself, then the employer or employers of such person shall not deduct the tax from the salary or wage payable to such person and such employer or employers, as the case may be, shall not be liable to pay tax on behalf of such person.  
  
As per Section 4A, In the case of an Employer liable to register but remained un-registered, the Employer shall not be liable to pay tax for a period of More than 4 years from the end of the year immediately preceding the year in which Registration Certificate is granted or proceeding for registration has been initiated, whichever is earlier, where Registration Certificate is granted on or after 1-4-2017.  
  
As per Section 4B, the State Government has been empowered to issue Notification to provide that the Notified organizations, shall deduct tax out of the commission payable to the agents. Accordingly, NotificationNo.PFT-2017/CR-20B/Taxation-3 dated 9-4-17, requires Insurance Companies registered under IRDA to deduct Profession Tax as per Entry 1A of Rs.2,500/-per year per person from the commission payable to Chief Agents, Principal Agents, Insurance Agents and Surveyors and Loss Assessors registered or licensed under the Insurance Act, 1938. The Insurer shall pay such deducted tax on or before the last date of the month in which such deduction is made. The provisions of filing of Returns, payment of tax, assessments etc under the PT Act related to an employer and employee shall mutatis mutandis apply to the Insurer liable to deduct the tax and the agents from whom such tax is to be deducted.  
  
The Insurance Agent has been granted an option to either continue or cancel his enrollment Certificate. If such agent continues to be enrolled and pay PT on his own under Sch. Entry 2(d) then he shall furnish the certificate prescribed  under Rule 9(A) of the PT Act to the Insurer. Where the agent receives commission from more than one Insurer. In such cases it shall be the responsibility of the agent to authorise one of the Insurer to deduct the tax from the commission paid or payable to him.  
  
As per Section 4C, the State Government has been empowered to issue Notification to Notify the person or class of persons (referred to as “tax collector”), who is eligible to receive any amount towards supply of goods or, as the case may be, supply of services or towards any other purposes, to collect an amount towards tax from an enrolled person or a person, liable to be enrolled. 

**VII. REGISTRATION AS EMPLOYER (PTRC)**

Every Employer liable to pay tax on behalf of the employees should obtain the Registration Certificate has to apply within 30 days from date of liability to obtain Enrollment Certificate.  
  
The Application for new Registration or amendment needs to be made in Form I and required to be filed electronically on Department website. An application having places of work within the jurisdiction of different prescribed authorities shall make an application for registration separately to each such authority in respect of his place of work, within the jurisdiction of that authority.  
  
With effect from 09-05-2018, an employer may, at his option make a single application to the prescribed authority for his principal place of work and declare other places of work as additional places of work. Similarly, an employer already holding more than one certificate of registration may, at this option apply for declaration of one of his place of work as principal place of work and apply for cancellation of certificates of registration granted for the other places of work, to the prescribed authority, under whose jurisdiction his place of work falls.  
  
The holder of the certificate of registration shall display conspicuously at his place of work the certificate of registration or a copy thereof. 

**VIII. RETURNS AND PAYMENT OF TAXES**

**Employer’s Liability to file return and payment of tax (from 6-6-2019)**

**Tax Liability**

**Periodicity**

**Months of salary to be covered**

**Due Date**

Less than 1,00,000/- in previous year

Annual

March of the previous year and April to February of the current year

31st March

1,00,000/- or more in previous year

Monthly

Salary of previous month

End of the month for which return is filed (E-Payment in MTR-6 and E-Return in Form-IIIB)

In case of first year of registration

Monthly

Salary of previous month

End of the month for which return is filed (E-Payment in MTR-6 and E-Return in Form-IIIB)

 Employer who has filed monthly returns for April and/or May 2019 needs to continue filing monthly return till March 2020 unless the periodicity is changed to yearly.  
  
**Employer’s Liability to file return and payment of tax (upto 5-6-2019)**

**Tax Liability**

**Periodicity**

**Months of salary to be covered**

**Due Date**

Less than 50,000/- in previous year

Annual

March of the previous year and April to February of the current year

31st March

50,000/- or more in previous year

Monthly

Salary of previous month

End of the month for which return is filed(E-Payment in MTR-6 and E-Return in Form-IIIB)

In case of first year of registration

Monthly

Salary of previous month

End of the month for which return is filed(E-Payment in MTR-6 and E-Return in Form-IIIB)

Filing of e-returns by Employers i.e. PTRC holders was made mandatory for monthly return filers from 1st Feb, 2011 and all the employers from 1st August, 2011.  
  
Employer may furnish a revised return _suo motu,_ within 6 months from end of the year or before a Notification for assessment is served on him, whichever is earlier. He may revise return based on intimation received, as a result of Business Audit or Search, within 30 days of such intimation.  
  
**Payment**  
  
Every Employer holding Profession Tax Registration Certificate (PTRC) shall pay Tax, Interest, Penalty or any amount due and payable by or under the said Act electronically in MTR-6 challan by selecting Form ID-III-B in the challan.

**IX. ASSESSMENT**

Previously there was no time limit for completion of assessment. However for any year starting on or after 1-4-2004, where all the returns are filed by the employer within one month from the end of the year to which such returns relate, no order of assessment in respect of that year shall be made after the expiry of three years from the end of the said year and where order is not made within the period aforesaid, the returns so filed shall be deemed to have been accepted as correct and complete.  
  
In the case of returns for the years ending on or before 31st March, 2004, if returns were filed before 30th September. 2004, order of assessment cannot be made after 1st April, 2007.  
  
Where it comes to the notice of the Commissioner that, any employer has not furnished returns in respect of any period by the prescribed date, the Commissioner may, at any time, before the expiry of the period of eight years from the end of the year to which such period relates, after giving such defaulting employer a reasonable opportunity of being heard in that matter, assess, to the best of his judgment, the tax, if any, due from such employer.  
  
Similarly, the Commissioner on being satisfied that an employer liable to pay tax in respect of any period, has failed to apply for registration within time as required by this Act, the Commissioner shall, at any time, before the expiry of the period of eight years from the end of the year to which such period relates, after giving such defaulting employer, a reasonable opportunity of being heard, assess, to the best of his judgment., the tax, if any, due from the employer in respect of that period, and any period or periods subsequent thereto.  
  
Assessed tax shall be paid within 15 days of receipt of the notice of demand.  
  
The provisions of the Maharashtra Goods and Services Tax Act, 2017 regarding recovery of tax shall, _mutatis mutandis_ apply for the recovery of dues under this Act. 

**X. PENALTIES AND INTEREST**

**Penalty for “late application” for Registration**  
  
The prescribed authority may impose penalty of Rs.5/- for each day of delay in case of an employer for registration and Rs.2/-  for each day of delay in case of any person for Enrolment, after giving a reasonable opportunity of hearing.  
  
**Interest for “late application” for Enrolment**  
  
The Person shall be liable to pay simple interest at the rate of 1.25% per month or part thereof of the amount of tax payable, from the 1st July of that year, till the date of payment of such tax.  
  
**Penalty for giving “false information in any application” for Enrolment/Registration**  
  
The authority may impose penalty equal to 3 times the tax payable under the Act, after giving hearing opportunity.  
  
**Late fee for “late filing of returns” by Employer**  
  
Where an employer fails to file return within the prescribed time, he shall pay, by way of a late fee, an amount of Rs.200/-in case he files the return within a period of 30 days after the expiry of the prescribed time for filing of such return and an amount of Rs.1,000/- in any other case.  
  
The State Government has issued various Notifications from time to time exempting the whole or part of any late fee payable by class or classes of employers subject to such conditions as prescribed.  
  
**Penalty for “Non-payment or late payment of tax” by Person/Employer**  
  
The authority may impose a penalty equal to 10% of the tax due, after giving opportunity of hearing.  
  
**Interest on late payment of tax by Employer or Enrolled Person**

1.  Interest for late payment of tax was 1.25% p.m. till 30-04-2017.
2.  With effect from 1-5-2017, Interest is chargeable @ 1.25% p.m. or part thereof for delay in payment of tax up to one month. Thereafter, for second and third month interest will be charged @ 1.5% p.m. or part thereof. From fourth month onward, interest will be chargeable @ 2% p.m. or part thereof. 

**Sr.**

**Period liable for Interest**

**Rate of Interest**

1

Delay up to one month

One and a quarter per cent of the amount of such unpaid tax, for the month or for part thereof.

2

Delay up to three months

         i.            Delay up to one month – One and a quarter per cent of the amount of such unpaid tax, for the month or for part thereof,

       ii.            Delay beyond one month up to three months – One and half per cent of the amount of such unpaid tax, for each month or for part there of or 2nd and 3rd Month

3

Delay more than three months

     iii.            Delay up to one month – One and a quarter per cent of the amount of such unpaid tax, for the month or for part thereof, 

     iv.            Delay beyond one month up to three months – One and half per cent of the amount of such unpaid tax, for each month or for part there off or 2nd and 3rd Month

       v.            Delay more than three months – Two per cent of the amount of such unpaid tax, for each month or for part there of from 4th Month.

The old rate of interest will apply where the default starts and ends before the 1st May 2017. In another words, if the tax has become due before 1st May 2017 and the default continues after the 1st May 2017, then for the period of default before 1st May 2017, the old rate of interest shall apply and in so far as the default continues on or after 1st May 2017, the new rates will apply as per slabs which shall commence on 1st May 2017.  
  
**Interest on late payment of assessment, appeal, revision and other dues**  
  
The interest for late payment of tax or any additional demand of tax raised in assessment, appeal or revision is 1.25% p.m..  
  
**Interest on refund of excess payment of tax**  
  
In respect of any period of assessment commencing on or after 1-4-2004, the interest @6% p.a. on refunds for the period commencing on the date next following the last date of the period of assessment to which such order relates and ending on the date of such order or for a period of 18 months, whichever is less.  
  
Where the refund of tax, whether in full or in part, includes any amount of refund on any payment of tax made after the date prescribed for filing of the last return for the period of assessment, then the interest, in so far as it relates to the refund arising from such payment, shall be calculated from the date of such payment to the date of such order.  
  
Where any refund by virtue of an order issued is not so refunded within 90 days from the date-of the order, a simple interest at the rate of 6% p.a. on the said amount from the date immediately following the expiry of the period of 90 days from the date of such order.  
  
**Penalty for non-compliance**  
  
The authority may impose a penalty not exceeding Rs.5,000/-, if fails to comply with any of the provisions of the Act & Rules. If the offence is a continuing one, with fine not exceeding Rs.50/- per day during the period of the continuance of the offence.  
  
**Recovery of Profession Tax Dues**  
  
The provisions of the Maharashtra Goods and Services Tax Act, 2017 regarding recovery of tax shall, mutatis mutandis apply for the recovery of dues under this Act.

**II. APPEAL, REVISION AND RECTIFICATION**

Any person or employer aggrieved by any order can file appeal against such order within 60 days from the date of receipt of demand notice or order in Form XVI. The appellate authority may admit the appeal after the expiry of the above period, if he is satisfied that there was sufficient cause for the delay.  
  
An application in Form XVI can be made for revision of any Appeal Order, Revision Order or Determination of Disputed Question Order passed within 60 days from the date of receipt of the order after giving the applicant or the assessee a reasonable opportunity of being heard.  
  
The Commissioner may, of his own motion, revise any order passed by any authority other than the Tribunal under this Act, however no order can be revised by the Commissioner after the expiry of three years from the passing of the impugned order and without giving the applicant or the assessee a reasonable opportunity of being heard.  
  
Any authority under the Act may, of his own motion or on an application being made in this behalf in Form XVII, rectify any mistake apparent on the face of the record or review his own order, if any employer has been under-assessed for any period. No order shall be passed after the expiry of three years from the passing of the impugned order and if an order has an adverse effect on an employer or a person, unless a reasonable opportunity of being heard has been given to such employer or person. 

**III. COMPOUNDING OF OFFENCES**

The Commissioner may, either before or after the institution of proceedings for an offence under this Act, permit any person charged with the offence to compound the offence on payment of such sum, not exceeding double the amount of tax to which the offence relates, as the Commissioner may determine.  
  
On payment of such sum no further proceedings shall be taken against the person in respect of the same offence.

###   
  
**SCHEDULE I  
_(See Section 3)_**  
  
**Schedule of rates of tax on professions, trades, callings and employments**

  
**Sr. No.  
1**

**Class of Persons  
2  
PART I**

**Rate of Tax ( )  
3**

**1.**

Salary and wage earners. Such persons whose monthly salaries or wages,

(a)

do not exceed 7,500

Nil

(b)

exceed 7,500 but do not exceed 10,000 – (Females)  
exceed 7,500 but do not exceed 10,000 – (Males)

Nil  
175 per month

(c)

exceed 10,000

2500 per anum, to be paid in the following manner:—

        a.            200/- per month except for the month February;

       b.            300/- for the month February

**1A.**

Commission paid to agents by Notified organizations

2500 per anum

**2.**

(a)

Legal Practitioners including Solicitors and Notaries;

(b)

Medical Practitioners, including Medical Consultants and Dentists;

(c)

Technical and Professional Consultants, including Architects, Engineers, R.C.C. Consultants, Tax Consultants, Chartered Accountants, Actuaries and Management Consultants;

(d)

Chief Agents, Principal Agents, Insurance Agents and Surveyors or Loss Assessors registered or licensed under the Insurance Act, 1938, U.T.I. Agents under U.T.I. Scheme, N.S.S. agents under postal Scheme;

(e)

Commission Agents, Dalals and Brokers (other than estate brokers covered by any other entry elsewhere in this Schedule);

(f)

All types of Contractors (other than building contractors covered by any other entry elsewhere in this Schedule); and

(g)

Diamond dressers and diamond polishers; having not less than one year’s standing in the profession.

2500 per annum

**3.**

(a)

Members of Association recognised under the Forward Contracts (Regulations) Act, 1952.

2500 per annum

(b)

         i. Member of Stock Exchanges recognised under the Security Contracts (Regulation) Act, 1956;

2500 per annum

       ii. Remisiers recognised by the Stock Exchange.

2500 per annum

**4.**

(a)

Building Contractors;

2500 per annum

(b)

Estate Agents, Brokers or Plumbers, having not less than one year’s standing in the profession

2500 per annum

**5.**

Directors (other than those nominated by Government) of Companies registered under the Companies Act, 1956, and Banking Companies as defined in the Banking Regulation Act, 1949.

2500 per annum

**_Explanation:_** The term ‘Directors’ for the purpose of this entry will not include the persons who are Directors of the companies whose registered offices are situated outside the State of Maharashtra and who are not residing in the State of Maharashtra.

**6.**

(a)

Bookmakers and Trainers licensed by the Royal Western India Turf Club Limited;

2500 per annum

(b)

Jockeys licensed by the said Club

2500 per annum

**7.**

Self-employed persons in the Motion Picture Industry, Theatre, Orchestra, Television, Modelling or Advertising Industries, as follows:

(a)

Writers, Lyricists, Directors, Actors and Actresses (excluding Junior Artists), Musicians, Play-back Singers, Cameramen, Recordists, Editors and Still-Photographers

2500 per annum

(b)

Junior Artists, Production Managers, Assistant Directors, Assistant Recordists, Assistant Editors and Dancers.

1000 per annum

**8.**

Dealers registered under the Maharashtra Value Added Tax Act, 2002, or Dealers registered only under the Central Sales Tax Act, 1956, whose annual turnover of sales or purchases, —

(i)

is 25 lakh or less

2000 per annum

(ii)

exceeds 25 lakh

2500 per annum

**9.**

Occupiers of Factories as defined in the Factories Act, 1948, who are not covered by Entry 8 above.

2500 per annum

**10.**

(1)

(A) Employers of establishments as defined in the Bombay Shops and Establishments Act, 1948, where their establishments are situated within an area to which the aforesaid Act applies, and who are not covered by Entry 8 —

Such employers of establishments,—

(a) where no employee is employed;

1000 per annum

(b) where not exceeding two employees are employed;

2000 per annum

(c) Where more than two employees are employed.

2500 per annum

(B) Employers of establishments as defined in the Bombay Shops and Establishments Act, 1948, where their establishments are not situated within an area to which the aforesaid Act applies, and who are not covered by entry 8 —

Such employers of establishment,—

(a) where no employee is employed;

500 per annum

(b) where not exceeding two employees are employed;

1000 per annum

(c) Where more than two employees are employed.

2500 per annum

(2)

Persons owning/running STD/ISD booths or Cyber Cafes, other than those owned or run by Government or by physically handicapped persons;

1000 per annum

(3)

Conductors of Video or Audio Parlours, Video or Audio Cassette Libraries, Video Game Parlours;

2500 per annum

(4)

Cable Operators, Film Distributors;

2500 per annum

(5)

Persons owning/running marriage halls, conference halls, beauty parlours, health centres, pool parlours;

2500 per annum

(6)

Persons running/conducting coaching classes of all types.

2500 per annum

**11.**

Owners or Lessees of Petrol/Diesel/Oil Pumps and Service Stations/Garages and Workshops of Automobiles.

2500 per annum

**12.**

Licensed Foreign Liquor Vendors and employers of Residential Hotels and Theatres as defined in the Bombay Shops and Establishments Act, 1948.

2500 per annum

**13.**

Holders of permits for Transport Vehicles granted under the Motor Vehicles Act, 1988, which are used or adopted to be used for hire or reward, where any such person holds permit or permits for,—

(a)

three wheeler goods vehicles, for each such vehicle;

750 per annum

(b)

any taxi, passenger car, for each such vehicle;

1000 per annum

(c)

     iii. goods vehicles other than those covered by (a);

1500 per annum

     iv. trucks or buses, for each such vehicle :

1500 per annum

Provided that the total tax payable by a holder under this entry shall not exceed 2,500 per annum.

**14.**

Money lenders licensed under the Bombay Money-lender Act, 1946.

2500 per annum

**15.**

Individuals or Institutions conducting Chit-Funds.

2500 per annum

**16.**

Co-operative Societies registered or deemed to be registered under the Maharashtra Co-operative Societies Act, 1960 and engaged in any profession, trade or calling —

(i)

State level Societies;

2500 per annum

(ii)

Co-operative sugar factories and spinning mills;

2500 per annum

(iii)

District level Societies;

750 per annum

(iv)

Handloom weavers co-operative societies;

500 per annum

(v)

All other co-operative societies not covered by clauses (i), (ii), (iii) and (iv) above.

750 per annum

**17.**

Banking Companies, as defined in the Banking Regulation Act, 1949.

2500 per annum

**18.**

Companies registered under the Companies Act, 1956 and engaged in any profession, trade or calling.

2500 per annum

**18A.**

Limited liability partnership, registered under the Limited Liability Partnership Act, 2008.

2500 per annum

**19(a)**

Each Partner of a firm (whether registered or not under the Indian Partnership Act, 1932) engaged in any profession, trade or calling.

2500 per annum

**19(b)**

Each partner of a limited liability partnership, registered under the  Limited Liability Partnership Act, 2008 engaged in any profession, trade or calling.

2500 per annum

**20.**

Each Coparcener (not being a minor) of a Hindu Undivided Family, which is engaged in any profession, trade or calling.

2500 per annum

**20A.**

Persons, registered under the Maharashtra Goods and Services Tax Act, 2017

2500 per anum

**21.**

Persons other than those mentioned in any of the preceding entries who are engaged in any profession, trade, calling or employment and in respect of whom a Notification is issued under the second proviso to sub-section (2) of section 3.

2500 per annum

**_NOTES:_**

1.  Notwithstanding anything contained in this Schedule, where a person is covered by more than one entry of this Schedule, the highest rate of tax specified under any of those entries shall be applicable in his case. This provision shall not be applicable to entry 16(iv) of the schedule.
2.  For the purposes of Entry 8 of the Schedule, the Profession Tax shall be calculated on the basis of the “turnover of sales or purchases” of the previous year. If there is no previous year for such dealer, the rate of Profession Tax shall be 2,000. The expressions “turnover of sales” or “turnover of purchases” shall have the same meaning as assigned to them, respectively, under the Maharashtra Value Added Tax Act, 2002. 

[Back to Top](#) jQuery(document).ready(function() { var offset = 220; var duration = 500; jQuery(window).scroll(function() { if (jQuery(this).scrollTop() > offset) { jQuery('.back-to-top').fadeIn(duration); } else { jQuery('.back-to-top').fadeOut(duration); } }); jQuery('.back-to-top').click(function(event) { event.preventDefault(); jQuery('html, body').animate({scrollTop: 0}, duration); return false; }) });

<!-- if(MSFPhover) { MSFPnav4n=MSFPpreload("../\_derived/back\_cmp\_Profile110\_back.gif"); MSFPnav4h=MSFPpreload("../\_derived/back\_cmp\_Profile110\_back\_a.gif"); } // -->[![Back](../_derived/back_cmp_Profile110_back.gif)](customs-act-1962.html) <!-- if(MSFPhover) { MSFPnav5n=MSFPpreload("../\_derived/home\_cmp\_Profile110\_home.gif"); MSFPnav5h=MSFPpreload("../\_derived/home\_cmp\_Profile110\_home\_a.gif"); } // -->[![Home](../_derived/home_cmp_Profile110_home.gif)](../index.htm) <!-- if(MSFPhover) { MSFPnav6n=MSFPpreload("../\_derived/up\_cmp\_Profile110\_up.gif"); MSFPnav6h=MSFPpreload("../\_derived/up\_cmp\_Profile110\_up\_a.gif"); } // -->[![Up](../_derived/up_cmp_Profile110_up.gif)](index-part4.html)

  ![](../images/footer.jpg)
